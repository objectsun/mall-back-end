package com.sun.mall.order.service;

import com.sun.mall.order.entity.OmsOrderSetting;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单配置信息 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface OmsOrderSettingService extends IService<OmsOrderSetting> {

}
