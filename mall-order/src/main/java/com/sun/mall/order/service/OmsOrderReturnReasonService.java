package com.sun.mall.order.service;

import com.sun.mall.order.entity.OmsOrderReturnReason;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 退货原因 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface OmsOrderReturnReasonService extends IService<OmsOrderReturnReason> {

}
