package com.sun.mall.order.service.impl;

import com.sun.mall.order.entity.OmsOrderSetting;
import com.sun.mall.order.mapper.OmsOrderSettingMapper;
import com.sun.mall.order.service.OmsOrderSettingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单配置信息 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class OmsOrderSettingServiceImpl extends ServiceImpl<OmsOrderSettingMapper, OmsOrderSetting> implements OmsOrderSettingService {

}
