package com.sun.mall.order.mapper;

import com.sun.mall.order.entity.OmsOrderReturnReason;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 退货原因 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface OmsOrderReturnReasonMapper extends BaseMapper<OmsOrderReturnReason> {

}
