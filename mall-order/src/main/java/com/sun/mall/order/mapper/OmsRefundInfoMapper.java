package com.sun.mall.order.mapper;

import com.sun.mall.order.entity.OmsRefundInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 退款信息 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface OmsRefundInfoMapper extends BaseMapper<OmsRefundInfo> {

}
