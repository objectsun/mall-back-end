package com.sun.mall.order.service;

import com.sun.mall.order.entity.OmsPaymentInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 支付信息表 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface OmsPaymentInfoService extends IService<OmsPaymentInfo> {

}
