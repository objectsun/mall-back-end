package com.sun.mall.order.service.impl;

import com.sun.mall.order.entity.OmsOrderReturnReason;
import com.sun.mall.order.mapper.OmsOrderReturnReasonMapper;
import com.sun.mall.order.service.OmsOrderReturnReasonService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 退货原因 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class OmsOrderReturnReasonServiceImpl extends ServiceImpl<OmsOrderReturnReasonMapper, OmsOrderReturnReason> implements OmsOrderReturnReasonService {

}
