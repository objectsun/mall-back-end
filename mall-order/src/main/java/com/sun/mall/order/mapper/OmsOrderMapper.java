package com.sun.mall.order.mapper;

import com.sun.mall.order.entity.OmsOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface OmsOrderMapper extends BaseMapper<OmsOrder> {

}
