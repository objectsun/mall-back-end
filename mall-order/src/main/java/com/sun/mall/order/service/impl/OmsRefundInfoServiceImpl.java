package com.sun.mall.order.service.impl;

import com.sun.mall.order.entity.OmsRefundInfo;
import com.sun.mall.order.mapper.OmsRefundInfoMapper;
import com.sun.mall.order.service.OmsRefundInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 退款信息 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class OmsRefundInfoServiceImpl extends ServiceImpl<OmsRefundInfoMapper, OmsRefundInfo> implements OmsRefundInfoService {

}
