package com.sun.mall.order.service;

import com.sun.mall.order.entity.OmsOrderReturnApply;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单退货申请 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface OmsOrderReturnApplyService extends IService<OmsOrderReturnApply> {

}
