package com.sun.mall.order.service;

import com.sun.mall.order.entity.OmsRefundInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 退款信息 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface OmsRefundInfoService extends IService<OmsRefundInfo> {

}
