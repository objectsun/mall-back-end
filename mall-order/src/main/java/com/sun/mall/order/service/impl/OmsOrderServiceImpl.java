package com.sun.mall.order.service.impl;

import com.sun.mall.order.entity.OmsOrder;
import com.sun.mall.order.mapper.OmsOrderMapper;
import com.sun.mall.order.service.OmsOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class OmsOrderServiceImpl extends ServiceImpl<OmsOrderMapper, OmsOrder> implements OmsOrderService {

}
