package com.sun.mall.order.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 退款信息 前端控制器
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@RestController
@RequestMapping("/order/oms-refund-info")
public class OmsRefundInfoController {

}

