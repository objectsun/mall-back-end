package com.sun.mall.order.service;

import com.sun.mall.order.entity.OmsOrderItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单项信息 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface OmsOrderItemService extends IService<OmsOrderItem> {

}
