package com.sun.mall.order.service.impl;

import com.sun.mall.order.entity.OmsOrderOperateHistory;
import com.sun.mall.order.mapper.OmsOrderOperateHistoryMapper;
import com.sun.mall.order.service.OmsOrderOperateHistoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单操作历史记录 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class OmsOrderOperateHistoryServiceImpl extends ServiceImpl<OmsOrderOperateHistoryMapper, OmsOrderOperateHistory> implements OmsOrderOperateHistoryService {

}
