package com.sun.mall.order.service.impl;

import com.sun.mall.order.entity.OmsOrderReturnApply;
import com.sun.mall.order.mapper.OmsOrderReturnApplyMapper;
import com.sun.mall.order.service.OmsOrderReturnApplyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单退货申请 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class OmsOrderReturnApplyServiceImpl extends ServiceImpl<OmsOrderReturnApplyMapper, OmsOrderReturnApply> implements OmsOrderReturnApplyService {

}
