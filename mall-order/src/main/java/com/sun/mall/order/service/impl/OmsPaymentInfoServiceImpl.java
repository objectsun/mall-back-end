package com.sun.mall.order.service.impl;

import com.sun.mall.order.entity.OmsPaymentInfo;
import com.sun.mall.order.mapper.OmsPaymentInfoMapper;
import com.sun.mall.order.service.OmsPaymentInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 支付信息表 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class OmsPaymentInfoServiceImpl extends ServiceImpl<OmsPaymentInfoMapper, OmsPaymentInfo> implements OmsPaymentInfoService {

}
