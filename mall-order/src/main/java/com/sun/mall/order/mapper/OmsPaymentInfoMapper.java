package com.sun.mall.order.mapper;

import com.sun.mall.order.entity.OmsPaymentInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 支付信息表 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface OmsPaymentInfoMapper extends BaseMapper<OmsPaymentInfo> {

}
