package com.sun.mall.order.mapper;

import com.sun.mall.order.entity.OmsOrderSetting;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单配置信息 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface OmsOrderSettingMapper extends BaseMapper<OmsOrderSetting> {

}
