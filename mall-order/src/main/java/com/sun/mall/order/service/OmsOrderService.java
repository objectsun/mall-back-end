package com.sun.mall.order.service;

import com.sun.mall.order.entity.OmsOrder;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface OmsOrderService extends IService<OmsOrder> {

}
