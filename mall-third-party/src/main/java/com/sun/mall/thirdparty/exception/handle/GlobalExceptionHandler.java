package com.sun.mall.thirdparty.exception.handle;


import com.sun.mall.thirdparty.common.Result;
import com.sun.mall.thirdparty.exception.BizRuntimeException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<Result<Object>> error(Exception e){
        e.printStackTrace();
        return ResponseEntity.ok(Result.error());
    }
    @ExceptionHandler(BizRuntimeException.class)
    @ResponseBody
    public ResponseEntity<Result<Object>> error(BizRuntimeException biz){
        log.error("handleBizRuntimeException: ", biz);
        return ResponseEntity.ok(
                Result.error(biz.getCode(), biz.getMessage(), biz.getData())
        );
    }
}