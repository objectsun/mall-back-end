package com.sun.mall.ware.service.impl;

import com.sun.mall.ware.entity.WmsWareInfo;
import com.sun.mall.ware.mapper.WmsWareInfoMapper;
import com.sun.mall.ware.service.WmsWareInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 仓库信息 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class WmsWareInfoServiceImpl extends ServiceImpl<WmsWareInfoMapper, WmsWareInfo> implements WmsWareInfoService {

}
