package com.sun.mall.ware.controller;


import com.sun.mall.ware.common.Result;
import com.sun.mall.ware.entity.WmsPurchase;
import com.sun.mall.ware.entity.WmsPurchaseDetail;
import com.sun.mall.ware.entity.WmsWareSku;
import com.sun.mall.ware.service.WmsPurchaseDetailService;
import com.sun.mall.ware.service.WmsPurchaseService;
import com.sun.mall.ware.vo.MergeVo;
import com.sun.mall.ware.vo.PurchaseDoneVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@RestController
@RequestMapping("/ware/wms-purchase-detail")
public class WmsPurchaseDetailController {
    @Autowired
    private WmsPurchaseDetailService purchaseDetailService;
    @GetMapping("/getPurchaseDetails")
    public Result<Object> getPurchaseDetails(Long wareId,Integer status,String key){
        List<WmsPurchaseDetail> purchaseDetails = purchaseDetailService.getPurchaseDetails(wareId,status,key);
        return Result.ok(purchaseDetails);
    }
    @PostMapping("/savePurchaseDetail")
    public Result<Object> savePurchaseDetail(@RequestBody WmsPurchaseDetail purchaseDetail){
        boolean result = purchaseDetailService.save(purchaseDetail);
        return Result.ok();
    }
    @PutMapping("/mergePurchase")
    public Result<Object> mergePurchase(@RequestBody MergeVo vo){
        boolean result = purchaseDetailService.mergePurchase(vo);
        return Result.ok();
    }
    @PutMapping("/donePurchase")
    public Result<Object> donePurchase(@RequestBody List<Long> purchaseIds){
        purchaseDetailService.donePurchase(purchaseIds);
        return Result.ok();
    }
}

