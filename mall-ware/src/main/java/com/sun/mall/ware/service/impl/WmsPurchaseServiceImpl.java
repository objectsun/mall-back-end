package com.sun.mall.ware.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.sun.mall.ware.constant.WareConstant;
import com.sun.mall.ware.entity.WmsPurchase;
import com.sun.mall.ware.entity.WmsPurchaseDetail;
import com.sun.mall.ware.mapper.WmsPurchaseMapper;
import com.sun.mall.ware.service.WmsPurchaseDetailService;
import com.sun.mall.ware.service.WmsPurchaseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 采购信息 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class WmsPurchaseServiceImpl extends ServiceImpl<WmsPurchaseMapper, WmsPurchase> implements WmsPurchaseService {

    @Autowired
    @Lazy
    private WmsPurchaseDetailService detailService;

    @Override
    public List<WmsPurchase> getPurchases(Integer status, String key) {
        List<WmsPurchase> purchases = this.list();
        purchases = purchases.stream().filter(purchaseDetail -> {
            boolean flag = true;
            if(status != null){
                flag = flag && purchaseDetail.getStatus().longValue() == status;
            }
            if(StrUtil.isNotBlank(key)){
                String id = purchaseDetail.getId() + "";
                flag = flag && id.equals(key);
            }
            return flag;
        }).collect(Collectors.toList());
        return purchases;
    }

    @Override
    public List<WmsPurchase> getUnreceivedPurchase() {
        List<WmsPurchase> purchases = this.list(Wrappers.<WmsPurchase>lambdaQuery().eq(WmsPurchase::getStatus,0).or().eq(WmsPurchase::getStatus,1));
        return purchases;
    }

    @Override
    public boolean assignPersonnel(WmsPurchase purchase) {
        boolean result = this.updateById(purchase);
        return true;
    }

    @Override
    public void receivePurchase(List<Long> purchaseIds) {
        List<WmsPurchase> purchases = purchaseIds.stream().map(id -> {
            WmsPurchase purchase = this.getById(id);
            return purchase;
        }).collect(Collectors.toList());
        purchases = purchases.stream().filter(purchase -> {
            if(purchase.getStatus() == WareConstant.PurcharseStatusEnum.CREATED.getCode() ||
            purchase.getStatus() == WareConstant.PurcharseStatusEnum.ASSIGNED.getCode()){
                return true;
            }
            return false;
        }).map(item -> {
            item.setStatus(WareConstant.PurcharseStatusEnum.RECEIVE.getCode());
            return item;
        }).collect(Collectors.toList());

        this.updateBatchById(purchases);

        purchases.forEach(item -> {
            List<WmsPurchaseDetail> purchaseDetails = detailService.list(Wrappers.<WmsPurchaseDetail>lambdaQuery().eq(WmsPurchaseDetail::getPurchaseId, item.getId()));
            purchaseDetails = purchaseDetails.stream().map(purchaseDetail -> {
                purchaseDetail.setStatus(WareConstant.PurcharseDetailStatusEnum.BUYING.getCode());
                return purchaseDetail;
            }).collect(Collectors.toList());
            detailService.updateBatchById(purchaseDetails);
        });

    }
}
