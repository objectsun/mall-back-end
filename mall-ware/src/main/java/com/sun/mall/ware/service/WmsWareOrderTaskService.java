package com.sun.mall.ware.service;

import com.sun.mall.ware.entity.WmsWareOrderTask;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 库存工作单 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface WmsWareOrderTaskService extends IService<WmsWareOrderTask> {

}
