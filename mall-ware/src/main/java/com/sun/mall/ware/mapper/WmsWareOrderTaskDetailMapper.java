package com.sun.mall.ware.mapper;

import com.sun.mall.ware.entity.WmsWareOrderTaskDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 库存工作单 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface WmsWareOrderTaskDetailMapper extends BaseMapper<WmsWareOrderTaskDetail> {

}
