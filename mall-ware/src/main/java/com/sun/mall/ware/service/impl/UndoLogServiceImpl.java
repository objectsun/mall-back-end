package com.sun.mall.ware.service.impl;

import com.sun.mall.ware.entity.UndoLog;
import com.sun.mall.ware.mapper.UndoLogMapper;
import com.sun.mall.ware.service.UndoLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class UndoLogServiceImpl extends ServiceImpl<UndoLogMapper, UndoLog> implements UndoLogService {

}
