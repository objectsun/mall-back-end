package com.sun.mall.ware.mapper;

import com.sun.mall.ware.entity.UndoLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface UndoLogMapper extends BaseMapper<UndoLog> {

}
