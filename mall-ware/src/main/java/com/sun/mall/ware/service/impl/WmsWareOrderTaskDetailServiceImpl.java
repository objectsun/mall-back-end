package com.sun.mall.ware.service.impl;

import com.sun.mall.ware.entity.WmsWareOrderTaskDetail;
import com.sun.mall.ware.mapper.WmsWareOrderTaskDetailMapper;
import com.sun.mall.ware.service.WmsWareOrderTaskDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 库存工作单 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class WmsWareOrderTaskDetailServiceImpl extends ServiceImpl<WmsWareOrderTaskDetailMapper, WmsWareOrderTaskDetail> implements WmsWareOrderTaskDetailService {

}
