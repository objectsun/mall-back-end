package com.sun.mall.ware.service;

import com.sun.mall.ware.entity.WmsPurchaseDetail;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sun.mall.ware.vo.MergeVo;
import com.sun.mall.ware.vo.PurchaseDoneVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface WmsPurchaseDetailService extends IService<WmsPurchaseDetail> {

    List<WmsPurchaseDetail> getPurchaseDetails(Long wareId, Integer status,String key);

    boolean mergePurchase(MergeVo vo);

//    void donePurchase(PurchaseDoneVo vo);
    void donePurchase(List<Long> purchaseIds);
}
