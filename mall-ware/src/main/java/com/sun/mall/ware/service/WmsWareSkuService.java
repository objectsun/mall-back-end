package com.sun.mall.ware.service;

import com.sun.mall.ware.entity.WmsWareSku;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品库存 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface WmsWareSkuService extends IService<WmsWareSku> {

    void addStock(Long skuId, Long wareId, Integer skuNum);
}
