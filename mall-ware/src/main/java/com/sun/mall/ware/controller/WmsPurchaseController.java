package com.sun.mall.ware.controller;


import com.sun.mall.ware.common.Result;
import com.sun.mall.ware.entity.WmsPurchase;
import com.sun.mall.ware.entity.WmsPurchaseDetail;
import com.sun.mall.ware.service.WmsPurchaseDetailService;
import com.sun.mall.ware.service.WmsPurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 采购信息 前端控制器
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@RestController
@RequestMapping("/ware/wms-purchase")
public class WmsPurchaseController {
    @Autowired
    private WmsPurchaseService purchaseService;
    @GetMapping("/getPurchases")
    public Result<Object> getPurchases(Integer status, String key){
        List<WmsPurchase> purchaseDetails = purchaseService.getPurchases(status,key);
        return Result.ok(purchaseDetails);
    }
    @GetMapping("/getUnreceivedPurchases")
    public Result<Object> getUnreceivedPurchases(){
        List<WmsPurchase> purchaseDetails = purchaseService.getUnreceivedPurchase();
        return Result.ok(purchaseDetails);
    }
    @PostMapping("/savePurchase")
    public Result<Object> savePurchase(@RequestBody WmsPurchase purchase){
        boolean result = purchaseService.save(purchase);
        return Result.ok();
    }
    @PostMapping("/assignPersonnel")
    public Result<Object> assignPersonnel(@RequestBody WmsPurchase purchase){
        boolean result = purchaseService.assignPersonnel(purchase);
        return Result.ok();
    }
    @PutMapping("/receivePurchase")
    public Result<Object> receivePurchase(@RequestBody List<Long> purchaseIds){
        purchaseService.receivePurchase(purchaseIds);
        return Result.ok();
    }
}

