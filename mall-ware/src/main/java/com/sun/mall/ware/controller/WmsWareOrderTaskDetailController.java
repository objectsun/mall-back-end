package com.sun.mall.ware.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 库存工作单 前端控制器
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@RestController
@RequestMapping("/ware/wms-ware-order-task-detail")
public class WmsWareOrderTaskDetailController {

}

