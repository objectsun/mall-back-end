package com.sun.mall.ware.controller;


import com.sun.mall.ware.common.Result;
import com.sun.mall.ware.entity.WmsWareInfo;
import com.sun.mall.ware.service.WmsWareInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 仓库信息 前端控制器
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@RestController
@RequestMapping("/ware/wms-ware-info")
public class WmsWareInfoController {
    @Autowired
    private WmsWareInfoService wareInfoService;
    @GetMapping("/getWareInfos")
    public Result<Object> getWareInfos(){
        List<WmsWareInfo> wareInfos = wareInfoService.list();
        return Result.ok(wareInfos);
    }
    @GetMapping("/getWareInfo")
    public Result<Object> getWareInfo(Long wareId){
        WmsWareInfo wareInfo = wareInfoService.getById(wareId);
        return Result.ok(wareInfo);
    }

    @PostMapping("/saveWareInfo")
    public Result<Object> saveWareInfo(@RequestBody WmsWareInfo wareInfo){
        boolean result = wareInfoService.save(wareInfo);
        return Result.ok();
    }

    @PutMapping("/updateWare")
    public Result<Object> updateWareInfo(@RequestBody WmsWareInfo wareInfo){
        boolean result = wareInfoService.updateById(wareInfo);
        return Result.ok();
    }

    @PostMapping("/deleteWareInfos")
    public Result<Object> deleteWareInfos(@RequestBody List<Long> ids){
        boolean result = wareInfoService.removeBatchByIds(ids);
        return Result.ok();
    }
}

