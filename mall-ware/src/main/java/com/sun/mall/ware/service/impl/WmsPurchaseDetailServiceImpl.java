package com.sun.mall.ware.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.sun.mall.ware.constant.WareConstant;
import com.sun.mall.ware.entity.WmsPurchase;
import com.sun.mall.ware.entity.WmsPurchaseDetail;
import com.sun.mall.ware.mapper.WmsPurchaseDetailMapper;
import com.sun.mall.ware.service.WmsPurchaseDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sun.mall.ware.service.WmsPurchaseService;
import com.sun.mall.ware.service.WmsWareSkuService;
import com.sun.mall.ware.vo.MergeVo;
import com.sun.mall.ware.vo.PurchaseDoneVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class WmsPurchaseDetailServiceImpl extends ServiceImpl<WmsPurchaseDetailMapper, WmsPurchaseDetail> implements WmsPurchaseDetailService {

    @Autowired
    @Lazy
    private WmsPurchaseService purchaseService;
    @Autowired
    @Lazy
    private WmsWareSkuService wareSkuService;
    @Override
    public List<WmsPurchaseDetail> getPurchaseDetails(Long wareId, Integer status,String key) {
        List<WmsPurchaseDetail> purchaseDetails = this.list();
        purchaseDetails = purchaseDetails.stream().filter(purchaseDetail -> {
            boolean flag = true;
            if(wareId != null){
                flag = flag && purchaseDetail.getWareId().longValue() == wareId;
            }
            if(status != null){
                flag = flag && purchaseDetail.getStatus().longValue() == status;
            }
            if(StrUtil.isNotBlank(key)){
                String id = purchaseDetail.getId() + "";
                flag = flag && id.equals(key);
            }
            return flag;
        }).collect(Collectors.toList());
        return purchaseDetails;
    }

    @Override
    @Transactional
    public boolean mergePurchase(MergeVo vo) {
        Long purchaseId = vo.getPurchaseId();
        if(purchaseId == null || purchaseId.longValue() == 0){
            WmsPurchase purchase = new WmsPurchase();
            purchase.setStatus(WareConstant.PurcharseStatusEnum.CREATED.getCode());
            purchase.setCreateTime(new Date());
            purchase.setUpdateTime(new Date());
            purchaseService.save(purchase);
            purchaseId = purchase.getId();
        }else {
            WmsPurchase purchase = purchaseService.getById(purchaseId);
            purchase.setStatus(WareConstant.PurcharseStatusEnum.CREATED.getCode());
            purchase.setCreateTime(new Date());
            purchase.setUpdateTime(new Date());
            purchaseService.updateById(purchase);
        }
        List<Long> items = vo.getItems();
        Long finalPurchaseId = purchaseId;
        List<WmsPurchaseDetail> purchaseDetails = items.stream().map(item -> {
            WmsPurchaseDetail purchaseDetail = new WmsPurchaseDetail();
            purchaseDetail.setId(item);
            purchaseDetail.setPurchaseId(finalPurchaseId);
            purchaseDetail.setStatus(WareConstant.PurcharseDetailStatusEnum.ASSIGNED.getCode());
            return purchaseDetail;
        }).collect(Collectors.toList());
        this.updateBatchById(purchaseDetails);
        return true;
    }

    @Override
    //我写的逻辑是：直接将采购单下对应的所以采购项的状态都改变为已完成（比较简单，为了页面能使用）
    public void donePurchase(List<Long> purchaseIds) {
        //老师的逻辑是：对提交上来的采购单以及采购项的状态进行改变
//    public void donePurchase(PurchaseDoneVo vo) {
        List<WmsPurchase> purchases = purchaseIds.stream().map(id -> {
            WmsPurchase purchase = purchaseService.getById(id);
            return purchase;
        }).collect(Collectors.toList());
        purchases = purchases.stream().filter(purchase -> {
            if(purchase.getStatus() == WareConstant.PurcharseStatusEnum.RECEIVE.getCode()){
                return true;
            }
            return false;
        }).map(item -> {
            item.setStatus(WareConstant.PurcharseStatusEnum.FINISH.getCode());
            return item;
        }).collect(Collectors.toList());

        purchaseService.updateBatchById(purchases);

        purchases.forEach(item -> {
            List<WmsPurchaseDetail> purchaseDetails = this.list(Wrappers.<WmsPurchaseDetail>lambdaQuery().eq(WmsPurchaseDetail::getPurchaseId, item.getId()));
            purchaseDetails = purchaseDetails.stream().map(purchaseDetail -> {
                purchaseDetail.setStatus(WareConstant.PurcharseDetailStatusEnum.FINISH.getCode());
                //进行入库
                wareSkuService.addStock(purchaseDetail.getSkuId(),purchaseDetail.getWareId(),purchaseDetail.getSkuNum());
                return purchaseDetail;
            }).collect(Collectors.toList());
            this.updateBatchById(purchaseDetails);
        });
    }
}
