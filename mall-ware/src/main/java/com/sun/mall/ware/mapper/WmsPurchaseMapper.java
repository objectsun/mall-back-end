package com.sun.mall.ware.mapper;

import com.sun.mall.ware.entity.WmsPurchase;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 采购信息 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface WmsPurchaseMapper extends BaseMapper<WmsPurchase> {

}
