package com.sun.mall.ware.service;

import com.sun.mall.ware.entity.WmsPurchase;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sun.mall.ware.entity.WmsPurchaseDetail;

import java.util.List;

/**
 * <p>
 * 采购信息 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface WmsPurchaseService extends IService<WmsPurchase> {

    List<WmsPurchase> getPurchases(Integer status, String key);

    List<WmsPurchase> getUnreceivedPurchase();

    boolean assignPersonnel(WmsPurchase purchase);

    void receivePurchase(List<Long> purchaseIds);
}
