package com.sun.mall.ware.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.sun.mall.ware.entity.WmsWareSku;
import com.sun.mall.ware.mapper.WmsWareSkuMapper;
import com.sun.mall.ware.service.WmsWareSkuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 商品库存 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class WmsWareSkuServiceImpl extends ServiceImpl<WmsWareSkuMapper, WmsWareSku> implements WmsWareSkuService {

    @Override
    public void addStock(Long skuId, Long wareId, Integer skuNum) {
        List<WmsWareSku> wareSkus = this.list(Wrappers.<WmsWareSku>lambdaQuery().eq(WmsWareSku::getWareId, wareId).eq(WmsWareSku::getSkuId, skuId));
        if(wareSkus == null || wareSkus.size() != 0){
            WmsWareSku wareSku = new WmsWareSku();
            wareSku.setSkuId(skuId);
            wareSku.setStock(skuNum);
            this.save(wareSku);
            return;
        }
        this.baseMapper.addStock(skuId,wareId,skuNum);
    }
}
