package com.sun.mall.ware.controller;


import com.sun.mall.ware.common.Result;
import com.sun.mall.ware.entity.WmsWareInfo;
import com.sun.mall.ware.entity.WmsWareSku;
import com.sun.mall.ware.service.WmsWareSkuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 商品库存 前端控制器
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@RestController
@RequestMapping("/ware/wms-ware-sku")
public class WmsWareSkuController {
    @Autowired
    private WmsWareSkuService wareSkuService;
    @GetMapping("/getWareSkus")
    public Result<Object> getWareSkus(){
        List<WmsWareSku> wareSkus = wareSkuService.list();
        return Result.ok(wareSkus);
    }
    @GetMapping("/getWareSku")
    public Result<Object> getWareSku(Long id){
        WmsWareSku wareSku = wareSkuService.getById(id);
        return Result.ok(wareSku);
    }
    @PostMapping("/saveWareSku")
    public Result<Object> saveWareSku(@RequestBody WmsWareSku wareSku){
        boolean result = wareSkuService.save(wareSku);
        return Result.ok();
    }
    @PutMapping("/updateWareSku")
    public Result<Object> updateWareSku(@RequestBody WmsWareSku wareSku){
        boolean result = wareSkuService.updateById(wareSku);
        return Result.ok();
    }
    @PostMapping("/deleteWareSkus")
    public Result<Object> deleteWareSkus(@RequestBody List<Long> ids){
        boolean result = wareSkuService.removeBatchByIds(ids);
        return Result.ok();
    }
}

