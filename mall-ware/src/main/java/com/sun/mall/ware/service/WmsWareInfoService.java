package com.sun.mall.ware.service;

import com.sun.mall.ware.entity.WmsWareInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 仓库信息 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface WmsWareInfoService extends IService<WmsWareInfo> {

}
