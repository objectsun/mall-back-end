package com.sun.mall.ware.service;

import com.sun.mall.ware.entity.UndoLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface UndoLogService extends IService<UndoLog> {

}
