package com.sun.mall.coupon.service;

import com.sun.mall.coupon.entity.SmsCouponHistory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 优惠券领取历史记录 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface SmsCouponHistoryService extends IService<SmsCouponHistory> {

}
