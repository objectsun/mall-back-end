package com.sun.mall.coupon.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 商品阶梯价格 前端控制器
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@RestController
@RequestMapping("/coupon/sms-sku-ladder")
public class SmsSkuLadderController {

}

