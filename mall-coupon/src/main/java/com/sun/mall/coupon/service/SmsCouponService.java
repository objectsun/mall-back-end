package com.sun.mall.coupon.service;

import com.sun.mall.coupon.entity.SmsCoupon;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 优惠券信息 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface SmsCouponService extends IService<SmsCoupon> {

}
