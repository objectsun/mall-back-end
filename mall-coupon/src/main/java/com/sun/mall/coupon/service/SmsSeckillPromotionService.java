package com.sun.mall.coupon.service;

import com.sun.mall.coupon.entity.SmsSeckillPromotion;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 秒杀活动 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface SmsSeckillPromotionService extends IService<SmsSeckillPromotion> {

}
