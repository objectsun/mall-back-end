package com.sun.mall.coupon.mapper;

import com.sun.mall.coupon.entity.SmsSkuLadder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品阶梯价格 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface SmsSkuLadderMapper extends BaseMapper<SmsSkuLadder> {

}
