package com.sun.mall.coupon.mapper;

import com.sun.mall.coupon.entity.SmsSpuBounds;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品spu积分设置 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface SmsSpuBoundsMapper extends BaseMapper<SmsSpuBounds> {

}
