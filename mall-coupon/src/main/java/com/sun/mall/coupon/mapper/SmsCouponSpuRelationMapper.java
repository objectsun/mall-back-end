package com.sun.mall.coupon.mapper;

import com.sun.mall.coupon.entity.SmsCouponSpuRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 优惠券与产品关联 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface SmsCouponSpuRelationMapper extends BaseMapper<SmsCouponSpuRelation> {

}
