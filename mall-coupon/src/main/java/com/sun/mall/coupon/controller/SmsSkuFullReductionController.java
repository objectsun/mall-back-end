package com.sun.mall.coupon.controller;


import com.sun.mall.coupon.common.Result;
import com.sun.mall.coupon.service.SmsSkuFullReductionService;
import com.sun.mall.coupon.service.SmsSpuBoundsService;
import com.sun.mall.coupon.to.SkuReductionTo;
import com.sun.mall.coupon.to.SpuBoundTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 商品满减信息 前端控制器
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@RestController
@RequestMapping("/coupon/sms-sku-full-reduction")
public class SmsSkuFullReductionController {
    @Autowired
    private SmsSkuFullReductionService skuFullReductionService;

    @PostMapping("/saveSkuReduction")
    public Result<Object> saveSkuReduction(@RequestBody SkuReductionTo to){
        skuFullReductionService.saveSkuReduction(to);
        return Result.ok();
    }
}

