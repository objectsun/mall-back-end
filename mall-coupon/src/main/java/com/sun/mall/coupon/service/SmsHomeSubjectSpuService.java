package com.sun.mall.coupon.service;

import com.sun.mall.coupon.entity.SmsHomeSubjectSpu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 专题商品 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface SmsHomeSubjectSpuService extends IService<SmsHomeSubjectSpu> {

}
