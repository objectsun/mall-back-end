package com.sun.mall.coupon.service;

import com.sun.mall.coupon.entity.SmsMemberPrice;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品会员价格 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface SmsMemberPriceService extends IService<SmsMemberPrice> {

}
