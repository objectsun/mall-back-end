package com.sun.mall.coupon.mapper;

import com.sun.mall.coupon.entity.SmsCouponHistory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 优惠券领取历史记录 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface SmsCouponHistoryMapper extends BaseMapper<SmsCouponHistory> {

}
