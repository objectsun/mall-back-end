package com.sun.mall.coupon.controller;


import com.sun.mall.coupon.common.Result;
import com.sun.mall.coupon.service.SmsSpuBoundsService;
import com.sun.mall.coupon.to.SpuBoundTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 商品spu积分设置 前端控制器
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@RestController
@RequestMapping("/coupon/sms-spu-bounds")
public class SmsSpuBoundsController {

    @Autowired
    private SmsSpuBoundsService spuBoundsService;

    @PostMapping("/saveSpuBounds")
    public Result<Object> saveSpuBounds(@RequestBody SpuBoundTo to){
        spuBoundsService.saveSpuBounds(to);
        return Result.ok();
    }
}

