package com.sun.mall.coupon.mapper;

import com.sun.mall.coupon.entity.SmsSeckillSkuRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 秒杀活动商品关联 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface SmsSeckillSkuRelationMapper extends BaseMapper<SmsSeckillSkuRelation> {

}
