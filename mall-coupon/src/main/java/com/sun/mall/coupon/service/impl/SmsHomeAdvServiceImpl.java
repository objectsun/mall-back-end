package com.sun.mall.coupon.service.impl;

import com.sun.mall.coupon.entity.SmsHomeAdv;
import com.sun.mall.coupon.mapper.SmsHomeAdvMapper;
import com.sun.mall.coupon.service.SmsHomeAdvService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 首页轮播广告 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class SmsHomeAdvServiceImpl extends ServiceImpl<SmsHomeAdvMapper, SmsHomeAdv> implements SmsHomeAdvService {

}
