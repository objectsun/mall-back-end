package com.sun.mall.coupon.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.sun.mall.coupon.controller.SmsSkuLadderController;
import com.sun.mall.coupon.entity.SmsMemberPrice;
import com.sun.mall.coupon.entity.SmsSkuFullReduction;
import com.sun.mall.coupon.entity.SmsSkuLadder;
import com.sun.mall.coupon.mapper.SmsSkuFullReductionMapper;
import com.sun.mall.coupon.service.SmsMemberPriceService;
import com.sun.mall.coupon.service.SmsSkuFullReductionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sun.mall.coupon.service.SmsSkuLadderService;
import com.sun.mall.coupon.to.MemberPrice;
import com.sun.mall.coupon.to.SkuReductionTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 商品满减信息 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class SmsSkuFullReductionServiceImpl extends ServiceImpl<SmsSkuFullReductionMapper, SmsSkuFullReduction> implements SmsSkuFullReductionService {

    @Autowired
    private SmsSkuLadderService skuLadderService;
    @Autowired
    private SmsMemberPriceService memberPriceService;


    @Override
    public void saveSkuReduction(SkuReductionTo to) {
        SmsSkuLadder skuLadder = new SmsSkuLadder();
        skuLadder.setSkuId(to.getSkuId());
        skuLadder.setFullCount(to.getFullCount());
        skuLadder.setDiscount(to.getDiscount());
        skuLadder.setAddOther(to.getCountStatus());
        skuLadderService.save(skuLadder);

        SmsSkuFullReduction skuFullReduction = new SmsSkuFullReduction();
        BeanUtil.copyProperties(to,skuFullReduction);
        this.save(skuFullReduction);

        List<MemberPrice> memberPrices = to.getMemberPrices();
        if(memberPrices != null && memberPrices.size() != 0){
            List<SmsMemberPrice> collect = memberPrices.stream().map(item -> {
                SmsMemberPrice smsMemberPrice = new SmsMemberPrice();
                smsMemberPrice.setSkuId(to.getSkuId());
                smsMemberPrice.setMemberLevelId(item.getId());
                smsMemberPrice.setMemberLevelName(item.getName());
                smsMemberPrice.setMemberPrice(item.getPrice());
                smsMemberPrice.setAddOther(1);
                return smsMemberPrice;
            }).collect(Collectors.toList());
            memberPriceService.saveBatch(collect);
        }
    }
}
