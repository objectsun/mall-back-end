package com.sun.mall.coupon.service.impl;

import com.sun.mall.coupon.entity.SmsMemberPrice;
import com.sun.mall.coupon.mapper.SmsMemberPriceMapper;
import com.sun.mall.coupon.service.SmsMemberPriceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品会员价格 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class SmsMemberPriceServiceImpl extends ServiceImpl<SmsMemberPriceMapper, SmsMemberPrice> implements SmsMemberPriceService {

}
