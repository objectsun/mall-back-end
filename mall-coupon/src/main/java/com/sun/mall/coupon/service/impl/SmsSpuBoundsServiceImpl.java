package com.sun.mall.coupon.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.sun.mall.coupon.entity.SmsSpuBounds;
import com.sun.mall.coupon.mapper.SmsSpuBoundsMapper;
import com.sun.mall.coupon.service.SmsSpuBoundsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sun.mall.coupon.to.SpuBoundTo;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品spu积分设置 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class SmsSpuBoundsServiceImpl extends ServiceImpl<SmsSpuBoundsMapper, SmsSpuBounds> implements SmsSpuBoundsService {

    @Override
    public void saveSpuBounds(SpuBoundTo to) {
        SmsSpuBounds spuBounds = new SmsSpuBounds();
        BeanUtil.copyProperties(to,spuBounds);
        this.save(spuBounds);
    }
}
