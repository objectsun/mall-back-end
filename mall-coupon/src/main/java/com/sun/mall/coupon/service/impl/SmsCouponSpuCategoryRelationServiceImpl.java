package com.sun.mall.coupon.service.impl;

import com.sun.mall.coupon.entity.SmsCouponSpuCategoryRelation;
import com.sun.mall.coupon.mapper.SmsCouponSpuCategoryRelationMapper;
import com.sun.mall.coupon.service.SmsCouponSpuCategoryRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优惠券分类关联 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class SmsCouponSpuCategoryRelationServiceImpl extends ServiceImpl<SmsCouponSpuCategoryRelationMapper, SmsCouponSpuCategoryRelation> implements SmsCouponSpuCategoryRelationService {

}
