package com.sun.mall.coupon.service;

import com.sun.mall.coupon.entity.SmsSeckillSkuRelation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 秒杀活动商品关联 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface SmsSeckillSkuRelationService extends IService<SmsSeckillSkuRelation> {

}
