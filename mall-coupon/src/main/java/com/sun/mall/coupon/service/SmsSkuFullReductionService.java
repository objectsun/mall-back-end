package com.sun.mall.coupon.service;

import com.sun.mall.coupon.entity.SmsSkuFullReduction;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sun.mall.coupon.to.SkuReductionTo;

/**
 * <p>
 * 商品满减信息 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface SmsSkuFullReductionService extends IService<SmsSkuFullReduction> {

    void saveSkuReduction(SkuReductionTo to);
}
