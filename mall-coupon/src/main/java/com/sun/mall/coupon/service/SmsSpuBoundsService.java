package com.sun.mall.coupon.service;

import com.sun.mall.coupon.entity.SmsSpuBounds;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sun.mall.coupon.to.SpuBoundTo;

/**
 * <p>
 * 商品spu积分设置 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface SmsSpuBoundsService extends IService<SmsSpuBounds> {

    void saveSpuBounds(SpuBoundTo to);
}
