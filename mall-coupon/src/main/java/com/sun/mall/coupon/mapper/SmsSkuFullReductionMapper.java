package com.sun.mall.coupon.mapper;

import com.sun.mall.coupon.entity.SmsSkuFullReduction;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品满减信息 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface SmsSkuFullReductionMapper extends BaseMapper<SmsSkuFullReduction> {

}
