package com.sun.mall.coupon.service.impl;

import com.sun.mall.coupon.entity.SmsSeckillSkuRelation;
import com.sun.mall.coupon.mapper.SmsSeckillSkuRelationMapper;
import com.sun.mall.coupon.service.SmsSeckillSkuRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 秒杀活动商品关联 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class SmsSeckillSkuRelationServiceImpl extends ServiceImpl<SmsSeckillSkuRelationMapper, SmsSeckillSkuRelation> implements SmsSeckillSkuRelationService {

}
