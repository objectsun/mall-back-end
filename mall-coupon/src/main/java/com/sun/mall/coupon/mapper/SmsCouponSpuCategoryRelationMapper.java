package com.sun.mall.coupon.mapper;

import com.sun.mall.coupon.entity.SmsCouponSpuCategoryRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 优惠券分类关联 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface SmsCouponSpuCategoryRelationMapper extends BaseMapper<SmsCouponSpuCategoryRelation> {

}
