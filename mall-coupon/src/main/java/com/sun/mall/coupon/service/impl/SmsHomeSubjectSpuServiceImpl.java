package com.sun.mall.coupon.service.impl;

import com.sun.mall.coupon.entity.SmsHomeSubjectSpu;
import com.sun.mall.coupon.mapper.SmsHomeSubjectSpuMapper;
import com.sun.mall.coupon.service.SmsHomeSubjectSpuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 专题商品 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class SmsHomeSubjectSpuServiceImpl extends ServiceImpl<SmsHomeSubjectSpuMapper, SmsHomeSubjectSpu> implements SmsHomeSubjectSpuService {

}
