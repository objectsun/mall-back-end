package com.sun.mall.coupon.service.impl;

import com.sun.mall.coupon.entity.SmsSeckillSession;
import com.sun.mall.coupon.mapper.SmsSeckillSessionMapper;
import com.sun.mall.coupon.service.SmsSeckillSessionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 秒杀活动场次 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class SmsSeckillSessionServiceImpl extends ServiceImpl<SmsSeckillSessionMapper, SmsSeckillSession> implements SmsSeckillSessionService {

}
