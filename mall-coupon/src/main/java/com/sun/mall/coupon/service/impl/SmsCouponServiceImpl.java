package com.sun.mall.coupon.service.impl;

import com.sun.mall.coupon.entity.SmsCoupon;
import com.sun.mall.coupon.mapper.SmsCouponMapper;
import com.sun.mall.coupon.service.SmsCouponService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优惠券信息 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class SmsCouponServiceImpl extends ServiceImpl<SmsCouponMapper, SmsCoupon> implements SmsCouponService {

}
