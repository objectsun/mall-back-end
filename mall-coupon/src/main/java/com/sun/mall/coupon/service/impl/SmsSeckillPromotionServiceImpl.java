package com.sun.mall.coupon.service.impl;

import com.sun.mall.coupon.entity.SmsSeckillPromotion;
import com.sun.mall.coupon.mapper.SmsSeckillPromotionMapper;
import com.sun.mall.coupon.service.SmsSeckillPromotionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 秒杀活动 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class SmsSeckillPromotionServiceImpl extends ServiceImpl<SmsSeckillPromotionMapper, SmsSeckillPromotion> implements SmsSeckillPromotionService {

}
