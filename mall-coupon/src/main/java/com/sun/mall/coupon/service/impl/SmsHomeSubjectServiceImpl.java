package com.sun.mall.coupon.service.impl;

import com.sun.mall.coupon.entity.SmsHomeSubject;
import com.sun.mall.coupon.mapper.SmsHomeSubjectMapper;
import com.sun.mall.coupon.service.SmsHomeSubjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 首页专题表【jd首页下面很多专题，每个专题链接新的页面，展示专题商品信息】 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class SmsHomeSubjectServiceImpl extends ServiceImpl<SmsHomeSubjectMapper, SmsHomeSubject> implements SmsHomeSubjectService {

}
