package com.sun.mall.coupon.controller;


import com.sun.mall.coupon.common.Result;
import com.sun.mall.coupon.entity.SmsCoupon;
import com.sun.mall.coupon.exception.BizRuntimeException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

/**
 * <p>
 * 优惠券信息 前端控制器
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@RestController
@RequestMapping("/coupon/sms-coupon")
public class SmsCouponController {
    @GetMapping("/member/list")
    public Result<Object> memberCoupons() {
        SmsCoupon smsCoupon = new SmsCoupon();
        smsCoupon.setCouponName("满100减10");
        return Result.ok(Arrays.asList(smsCoupon));
    }
}

