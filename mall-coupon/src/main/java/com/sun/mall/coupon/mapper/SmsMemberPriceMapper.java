package com.sun.mall.coupon.mapper;

import com.sun.mall.coupon.entity.SmsMemberPrice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品会员价格 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface SmsMemberPriceMapper extends BaseMapper<SmsMemberPrice> {

}
