package com.sun.mall.coupon.mapper;

import com.sun.mall.coupon.entity.SmsSeckillSession;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 秒杀活动场次 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface SmsSeckillSessionMapper extends BaseMapper<SmsSeckillSession> {

}
