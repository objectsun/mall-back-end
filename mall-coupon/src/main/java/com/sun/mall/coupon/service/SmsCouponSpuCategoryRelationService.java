package com.sun.mall.coupon.service;

import com.sun.mall.coupon.entity.SmsCouponSpuCategoryRelation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 优惠券分类关联 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface SmsCouponSpuCategoryRelationService extends IService<SmsCouponSpuCategoryRelation> {

}
