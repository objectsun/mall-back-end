package com.sun.mall.coupon.mapper;

import com.sun.mall.coupon.entity.SmsCoupon;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 优惠券信息 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface SmsCouponMapper extends BaseMapper<SmsCoupon> {

}
