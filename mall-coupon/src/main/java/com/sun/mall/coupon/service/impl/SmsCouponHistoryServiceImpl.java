package com.sun.mall.coupon.service.impl;

import com.sun.mall.coupon.entity.SmsCouponHistory;
import com.sun.mall.coupon.mapper.SmsCouponHistoryMapper;
import com.sun.mall.coupon.service.SmsCouponHistoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优惠券领取历史记录 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class SmsCouponHistoryServiceImpl extends ServiceImpl<SmsCouponHistoryMapper, SmsCouponHistory> implements SmsCouponHistoryService {

}
