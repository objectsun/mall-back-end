package com.sun.mall.coupon.mapper;

import com.sun.mall.coupon.entity.SmsSeckillSkuNotice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 秒杀商品通知订阅 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface SmsSeckillSkuNoticeMapper extends BaseMapper<SmsSeckillSkuNotice> {

}
