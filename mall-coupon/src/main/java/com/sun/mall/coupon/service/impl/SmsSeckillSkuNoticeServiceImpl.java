package com.sun.mall.coupon.service.impl;

import com.sun.mall.coupon.entity.SmsSeckillSkuNotice;
import com.sun.mall.coupon.mapper.SmsSeckillSkuNoticeMapper;
import com.sun.mall.coupon.service.SmsSeckillSkuNoticeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 秒杀商品通知订阅 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class SmsSeckillSkuNoticeServiceImpl extends ServiceImpl<SmsSeckillSkuNoticeMapper, SmsSeckillSkuNotice> implements SmsSeckillSkuNoticeService {

}
