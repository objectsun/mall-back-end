package com.sun.mall.coupon.mapper;

import com.sun.mall.coupon.entity.SmsHomeAdv;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 首页轮播广告 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface SmsHomeAdvMapper extends BaseMapper<SmsHomeAdv> {

}
