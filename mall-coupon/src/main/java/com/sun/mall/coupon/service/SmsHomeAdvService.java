package com.sun.mall.coupon.service;

import com.sun.mall.coupon.entity.SmsHomeAdv;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 首页轮播广告 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface SmsHomeAdvService extends IService<SmsHomeAdv> {

}
