package com.sun.mall.coupon.service.impl;

import com.sun.mall.coupon.entity.SmsCouponSpuRelation;
import com.sun.mall.coupon.mapper.SmsCouponSpuRelationMapper;
import com.sun.mall.coupon.service.SmsCouponSpuRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优惠券与产品关联 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class SmsCouponSpuRelationServiceImpl extends ServiceImpl<SmsCouponSpuRelationMapper, SmsCouponSpuRelation> implements SmsCouponSpuRelationService {

}
