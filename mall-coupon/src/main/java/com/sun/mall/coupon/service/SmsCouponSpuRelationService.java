package com.sun.mall.coupon.service;

import com.sun.mall.coupon.entity.SmsCouponSpuRelation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 优惠券与产品关联 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface SmsCouponSpuRelationService extends IService<SmsCouponSpuRelation> {

}
