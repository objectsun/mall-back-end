package com.sun.mall.search;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sun.mall.search.config.MallElasticSearchConfig;
import lombok.Data;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.Avg;
import org.elasticsearch.search.aggregations.metrics.AvgAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.naming.directory.SearchResult;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@SpringBootTest
class MallSearchApplicationTests {

    @Autowired
    private RestHighLevelClient client;
    @Test
    void indexData() throws IOException {
        IndexRequest indexRequest = new IndexRequest("users");
//        indexRequest.id("1").source("username","zhangsan","age",18,"gender","男");
        User user = new User();
        user.setUsername("alex");
        user.setPassword("777");
        user.setAge(18);
        user.setAccount(3000L);
        String jsonString = JSON.toJSONString(user);
        indexRequest.id("2").source(jsonString, XContentType.JSON);

        IndexResponse index = client.index(indexRequest, MallElasticSearchConfig.COMMON_OPTIONS);
        System.out.println(index);
    }

    @Test
    void searchData() throws IOException {
        SearchRequest searchRequest = new SearchRequest();
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
//        sourceBuilder.query(QueryBuilders.matchQuery("username","tyro"));
        sourceBuilder.query(QueryBuilders.matchAllQuery());
        TermsAggregationBuilder ageAgg = AggregationBuilders.terms("ageAgg").field("age").size(10);
        sourceBuilder.aggregation(ageAgg);
        AvgAggregationBuilder avgAgg = AggregationBuilders.avg("accAgg").field("account");
        sourceBuilder.aggregation(avgAgg);
        searchRequest.indices("users").source(sourceBuilder);
        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);

        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            String sourceAsString = hit.getSourceAsString();
            User user = JSON.parseObject(sourceAsString,MallSearchApplicationTests.User.class);
//            System.out.println(user.toString());
        }

        List<Aggregation> aggregations = response.getAggregations().asList();
//        for (Aggregation aggregation : aggregations) {
//            System.out.println(aggregation.getMetaData());
//        }
        Aggregations responseAggregations = response.getAggregations();
        Terms ageAgg1 = responseAggregations.get("ageAgg");
        List<? extends Terms.Bucket> buckets = ageAgg1.getBuckets();
        for (Terms.Bucket bucket : buckets) {
            System.out.println(bucket.getKey());
        }


        Avg accAgg = response.getAggregations().get("accAgg");
        System.out.println(accAgg.getValue());
    }

    @Test
    void deleteData() throws IOException {
        DeleteRequest deleteRequest = new DeleteRequest();
        deleteRequest.id("hcU1aYYB3gPc-SG9ID8f").index("users");
        DeleteResponse response = client.delete(deleteRequest, RequestOptions.DEFAULT);
        System.out.println(response);
    }

    @Test
    void updateData() throws IOException {
//        UpdateRequest updateRequest = new UpdateRequest();
//        updateRequest.ind
//        System.out.println();
    }

    @Data
    static class User{
        private String username;
        private String password;
        private Integer age;
        private Long account;
    }

    @Test
    void contextLoads() {
        System.out.println(client);
    }

}
