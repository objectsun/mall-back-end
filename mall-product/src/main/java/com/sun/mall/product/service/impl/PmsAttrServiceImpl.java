package com.sun.mall.product.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.sun.mall.product.common.ResultCode;
import com.sun.mall.product.entity.PmsAttr;
import com.sun.mall.product.entity.PmsAttrAttrgroupRelation;
import com.sun.mall.product.entity.PmsAttrGroup;
import com.sun.mall.product.exception.BizRuntimeException;
import com.sun.mall.product.mapper.PmsAttrAttrgroupRelationMapper;
import com.sun.mall.product.mapper.PmsAttrGroupMapper;
import com.sun.mall.product.mapper.PmsAttrMapper;
import com.sun.mall.product.mapper.PmsCategoryMapper;
import com.sun.mall.product.service.PmsAttrAttrgroupRelationService;
import com.sun.mall.product.service.PmsAttrService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sun.mall.product.vo.AttrVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 商品属性 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class PmsAttrServiceImpl extends ServiceImpl<PmsAttrMapper, PmsAttr> implements PmsAttrService {

    @Autowired
    private PmsAttrAttrgroupRelationMapper attrAttrgroupRelationMapper;
    @Autowired
    private PmsAttrGroupMapper attrGroupMapper;
    @Autowired
    private PmsCategoryMapper categoryMapper;


    @Override
    @Transactional
    public boolean saveAttr(AttrVo attrVo) {
        PmsAttr attr = new PmsAttr();
        BeanUtil.copyProperties(attrVo,attr);
        boolean result = this.save(attr);
        if(!result){
            throw BizRuntimeException.from(ResultCode.ERROR,"保存属性失败");
        }
        PmsAttrAttrgroupRelation attrAttrgroupRelation = new PmsAttrAttrgroupRelation();
        attrAttrgroupRelation.setAttrGroupId(attrVo.getAttrGroupId());
        attrAttrgroupRelation.setAttrId(attr.getAttrId());
        int insert = attrAttrgroupRelationMapper.insert(attrAttrgroupRelation);
        if(insert <= 0){
            throw BizRuntimeException.from(ResultCode.ERROR,"保存属性失败");
        }
        return true;
    }

    @Override
    public List<AttrVo> listByCatId(String type,String key, Long catId) {
//        List<PmsAttr> attrs = new ArrayList<>();
//        List<AttrVo> result = new ArrayList<>();
//        QueryWrapper<PmsAttr> wrapper = new QueryWrapper<>();
//        if (catId == 0 && StringUtils.isBlank(key)){
//            attrs =  this.list(wrapper);
//            BeanUtil.copyProperties(attrs,result);
//        }
//        if (catId != 0){
//            wrapper.eq("catelog_id",catId);
//        }
//        if (StringUtils.isNotBlank(key)){
//            wrapper.and(q -> {
//                q.like("attr_id",key)
//                        .or().like("attr_id",key)
//                        .or().like("attr_name",key);
//            });
//        }
//        attrs = this.list(wrapper);
//        BeanUtil.copyProperties(attrs,result);
        int attrType = type.contains("base") ? 1 : 0;
        List<AttrVo> result = this.baseMapper.listByCatId(attrType,key,catId);
        return result;
    }

    @Override
    public AttrVo getAttr(Long attrId) {
        PmsAttr attr = this.getById(attrId);
        AttrVo attrVo = new AttrVo();
        BeanUtil.copyProperties(attr,attrVo);
        PmsAttrAttrgroupRelation attrAttrgroupRelation = attrAttrgroupRelationMapper.selectOne(Wrappers.<PmsAttrAttrgroupRelation>lambdaQuery().eq(PmsAttrAttrgroupRelation::getAttrId, attrId));
        if (attrAttrgroupRelation != null){
            attrVo.setAttrGroupId(attrAttrgroupRelation.getAttrGroupId());
        }
        return attrVo;
    }

    @Override
    public boolean updateAttr(AttrVo attrVo) {
        PmsAttr attr = new PmsAttr();
        BeanUtil.copyProperties(attrVo,attr);
        boolean result = this.updateById(attr);
        if(!result){
            throw BizRuntimeException.from(ResultCode.ERROR,"修改属性失败");
        }
        PmsAttrAttrgroupRelation attrAttrgroupRelation = new PmsAttrAttrgroupRelation();
        attrAttrgroupRelation.setAttrGroupId(attrVo.getAttrGroupId());
        attrAttrgroupRelation.setAttrId(attr.getAttrId());
        int update = attrAttrgroupRelationMapper.update(attrAttrgroupRelation, Wrappers.<PmsAttrAttrgroupRelation>lambdaUpdate().eq(PmsAttrAttrgroupRelation::getAttrId, attr.getAttrId()));
        if(update <= 0){
            throw BizRuntimeException.from(ResultCode.ERROR,"修改属性失败");
        }
        return true;
    }
}
