package com.sun.mall.product.mapper;

import com.sun.mall.product.entity.PmsProductAttrValue;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * spu属性值 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface PmsProductAttrValueMapper extends BaseMapper<PmsProductAttrValue> {

}
