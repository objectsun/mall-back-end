package com.sun.mall.product.service.impl;

import com.sun.mall.product.common.ResultCode;
import com.sun.mall.product.entity.PmsSpuInfoDesc;
import com.sun.mall.product.exception.BizRuntimeException;
import com.sun.mall.product.mapper.PmsSpuInfoDescMapper;
import com.sun.mall.product.service.PmsSpuInfoDescService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * spu信息介绍 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class PmsSpuInfoDescServiceImpl extends ServiceImpl<PmsSpuInfoDescMapper, PmsSpuInfoDesc> implements PmsSpuInfoDescService {

    @Override
    public void saveSpuInfoDesc(PmsSpuInfoDesc spuInfoDesc) {
        this.save(spuInfoDesc);
    }
}
