package com.sun.mall.product.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.sun.mall.product.common.ResultCode;
import com.sun.mall.product.entity.PmsAttr;
import com.sun.mall.product.entity.PmsAttrAttrgroupRelation;
import com.sun.mall.product.entity.PmsAttrGroup;
import com.sun.mall.product.exception.BizRuntimeException;
import com.sun.mall.product.mapper.PmsAttrAttrgroupRelationMapper;
import com.sun.mall.product.mapper.PmsAttrGroupMapper;
import com.sun.mall.product.mapper.PmsAttrMapper;
import com.sun.mall.product.mapper.PmsCategoryMapper;
import com.sun.mall.product.service.PmsAttrGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sun.mall.product.service.PmsAttrService;
import com.sun.mall.product.vo.AttrGroupWithAttrsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <p>
 * 属性分组 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class PmsAttrGroupServiceImpl extends ServiceImpl<PmsAttrGroupMapper, PmsAttrGroup> implements PmsAttrGroupService {

    @Autowired
    private PmsAttrMapper attrMapper;
    @Autowired
    private PmsAttrAttrgroupRelationMapper attrAttrgroupRelationMapper;

    @Override
    public List<PmsAttrGroup> queryAttrGroupBycatelogId(String key, Long catelogId) {
        List<PmsAttrGroup> attrGroups = new ArrayList<>();
        QueryWrapper<PmsAttrGroup> wrapper = new QueryWrapper<>();
        if (catelogId == 0 && StringUtils.isBlank(key)){
            return this.list(wrapper);
        }
        if (catelogId != 0){
            wrapper.eq("catelog_id",catelogId);
        }
        if (StringUtils.isNotBlank(key)){
            wrapper.and(q -> {
                q.like("attr_group_id",key)
                        .or().like("attr_group_id",key)
                        .or().like("attr_group_name",key);
            });
        }
        attrGroups = this.list(wrapper);
        return attrGroups;
    }

    @Override
    public List<PmsAttr> getRelatedAttributions(Long attrGroupId) {
        List<PmsAttrAttrgroupRelation> attrAttrgroupRelations = attrAttrgroupRelationMapper.selectList(Wrappers.<PmsAttrAttrgroupRelation>lambdaQuery().eq(PmsAttrAttrgroupRelation::getAttrGroupId, attrGroupId));
        if(attrAttrgroupRelations == null || attrAttrgroupRelations.size() == 0){
            return new ArrayList<PmsAttr>();
        }
        List<Long> attrIds = attrAttrgroupRelations.stream().map(attrAttrgroupRelation -> attrAttrgroupRelation.getAttrId()).collect(Collectors.toList());
        List<PmsAttr> attrs = attrMapper.selectBatchIds(attrIds);
        return attrs;
    }

    @Override
    @Transactional
    public boolean deleteRelationWithAttr(Long attrId,Long attrGroupId) {
        int delete = attrAttrgroupRelationMapper.delete(Wrappers.<PmsAttrAttrgroupRelation>lambdaQuery().eq(PmsAttrAttrgroupRelation::getAttrId, attrId).eq(PmsAttrAttrgroupRelation::getAttrGroupId,attrGroupId));
        if(delete <= 0){
            throw BizRuntimeException.from(ResultCode.ERROR,"删除关系失败");
        }
        return true;
    }

    @Override
    public List<PmsAttr> getEnabledRelationAttr(Long attrGroupId, String key) {
        List<PmsAttrAttrgroupRelation> attrAttrgroupRelations = attrAttrgroupRelationMapper.selectList(Wrappers.<PmsAttrAttrgroupRelation>lambdaQuery().eq(PmsAttrAttrgroupRelation::getAttrGroupId, attrGroupId));
        if (attrAttrgroupRelations == null || attrAttrgroupRelations.size() == 0){
            return attrMapper.selectList(null);
        }
        List<Long> attrIds = attrAttrgroupRelations.stream().map(attrAttrgroupRelation -> attrAttrgroupRelation.getAttrId()).collect(Collectors.toList());
        List<PmsAttr> relatedAttrs = attrMapper.selectBatchIds(attrIds);
        List<PmsAttr> allAttrs = attrMapper.selectList(Wrappers.<PmsAttr>lambdaQuery().eq(PmsAttr::getAttrType,"1"));
        List<PmsAttr> result = allAttrs.stream().filter(attr -> {
            boolean flag = true;
            for (PmsAttr relatedAttr : relatedAttrs) {
                if(relatedAttr.getAttrId() == attr.getAttrId()){
                    flag = false;
                    break;
                }
            }
            return flag;
        }).collect(Collectors.toList());
        if(StringUtils.isNotBlank(key)){
            result = allAttrs.stream().filter(attr -> ((attr.getAttrId() + "").equals(key) || attr.getAttrName().equals(key))).collect(Collectors.toList());
        }
        return result;
    }

    @Override
    public boolean deleteBatchRelationWithAttr(List<PmsAttrAttrgroupRelation> attrAttrgroupRelations) {
        List<PmsAttrAttrgroupRelation> exitRelations = attrAttrgroupRelationMapper.selectList(Wrappers.<PmsAttrAttrgroupRelation>lambdaQuery().eq(PmsAttrAttrgroupRelation::getAttrGroupId, attrAttrgroupRelations.get(0).getAttrGroupId()));
        exitRelations = exitRelations.stream().filter(exitRelation -> {
            boolean flag = false;
            for (PmsAttrAttrgroupRelation attrAttrgroupRelation : attrAttrgroupRelations) {
                if (attrAttrgroupRelation.getAttrId() == exitRelation.getAttrId()){
                    flag = true;
                    break;
                }
            }
            return flag;
        }).collect(Collectors.toList());
        int ids = attrAttrgroupRelationMapper.deleteBatchIds(exitRelations);
        if(ids <= 0){
            throw BizRuntimeException.from(ResultCode.ERROR,"删除关联失败");
        }
        return true;
    }

    @Override
    public List<AttrGroupWithAttrsVo> attrGroupWithAttr(Long catId) {
        List<PmsAttrGroup> attrGroups = this.list(Wrappers.<PmsAttrGroup>lambdaQuery().eq(PmsAttrGroup::getCatelogId,catId));
        List<Long> attrGroupIds = attrGroups.stream().map(attrGroup -> attrGroup.getAttrGroupId()).collect(Collectors.toList());
        if(attrGroupIds == null || attrGroupIds.size() == 0){
            return new ArrayList<>();
        }
        List<PmsAttrAttrgroupRelation> attrAttrgroupRelations = attrAttrgroupRelationMapper.selectList(Wrappers.<PmsAttrAttrgroupRelation>lambdaQuery().in(PmsAttrAttrgroupRelation::getAttrGroupId, attrGroupIds));
        List<Long> attrIds = attrAttrgroupRelations.stream().map(attrAttrgroupRelation -> attrAttrgroupRelation.getAttrId()).collect(Collectors.toList());
        List<PmsAttr> attrs = attrMapper.selectBatchIds(attrIds);
        List<AttrGroupWithAttrsVo> result = attrGroups.stream().map(attrGroup -> {
            AttrGroupWithAttrsVo attrGroupWithAttrsVo = new AttrGroupWithAttrsVo();
            BeanUtil.copyProperties(attrGroup,attrGroupWithAttrsVo);
            return attrGroupWithAttrsVo;
        }).collect(Collectors.toList());
        result = result.stream().map(item -> {
            List<PmsAttrAttrgroupRelation> pmsAttrAttrgroupRelations = new ArrayList<>();
            List<PmsAttr> compareAttrs = new ArrayList<>();
            for (PmsAttrAttrgroupRelation attrAttrgroupRelation : attrAttrgroupRelations) {
                for (PmsAttr attr : attrs) {
                    if(attrAttrgroupRelation.getAttrId() == attr.getAttrId()){
                        compareAttrs.add(attr);
                        break;
                    }
                }
            }
            attrAttrgroupRelations.forEach(attrAttrgroupRelation -> {
                if(attrAttrgroupRelation.getAttrGroupId() == item.getAttrGroupId()){
                    pmsAttrAttrgroupRelations.add(attrAttrgroupRelation);
                }
            });
            List<PmsAttr> pmsAttrs = new ArrayList<>();
            for (PmsAttrAttrgroupRelation attrAttrgroupRelation : pmsAttrAttrgroupRelations) {
                for (PmsAttr attr : compareAttrs) {
                    if(attrAttrgroupRelation.getAttrId() == attr.getAttrId()){
                        pmsAttrs.add(attr);
                        break;
                    }
                }
            }
            item.setAttrList(pmsAttrs);
            return item;
        }).collect(Collectors.toList());
        return result;
    }
}
