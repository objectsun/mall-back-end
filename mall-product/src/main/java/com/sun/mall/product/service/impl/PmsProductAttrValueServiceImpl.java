package com.sun.mall.product.service.impl;

import com.sun.mall.product.entity.PmsProductAttrValue;
import com.sun.mall.product.mapper.PmsProductAttrValueMapper;
import com.sun.mall.product.service.PmsProductAttrValueService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * spu属性值 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class PmsProductAttrValueServiceImpl extends ServiceImpl<PmsProductAttrValueMapper, PmsProductAttrValue> implements PmsProductAttrValueService {

}
