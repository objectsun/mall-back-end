package com.sun.mall.product.service.impl;

import com.sun.mall.product.entity.PmsSkuSaleAttrValue;
import com.sun.mall.product.mapper.PmsSkuSaleAttrValueMapper;
import com.sun.mall.product.service.PmsSkuSaleAttrValueService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * sku销售属性&值 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class PmsSkuSaleAttrValueServiceImpl extends ServiceImpl<PmsSkuSaleAttrValueMapper, PmsSkuSaleAttrValue> implements PmsSkuSaleAttrValueService {

}
