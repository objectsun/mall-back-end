package com.sun.mall.product.service;

import com.sun.mall.product.entity.PmsBrand;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 品牌 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface PmsBrandService extends IService<PmsBrand> {

    List<PmsBrand> queryBrandsForPage(String key);

    boolean updateBrand(PmsBrand brand);
}
