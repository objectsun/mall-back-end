package com.sun.mall.product.service;

import com.sun.mall.product.entity.PmsAttr;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sun.mall.product.vo.AttrVo;

import java.util.List;

/**
 * <p>
 * 商品属性 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface PmsAttrService extends IService<PmsAttr> {

    boolean saveAttr(AttrVo attrVo);

    List<AttrVo> listByCatId(String type,String key, Long catId);

    AttrVo getAttr(Long attrId);

    boolean updateAttr(AttrVo attrVo);
}
