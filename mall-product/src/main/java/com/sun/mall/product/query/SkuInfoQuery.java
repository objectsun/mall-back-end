package com.sun.mall.product.query;

import lombok.Data;

import java.math.BigDecimal;
@Data
public class SkuInfoQuery {
    private Long catelogId;

    private Long brandId;

    private String key;

    private BigDecimal min;

    private BigDecimal max;
}
