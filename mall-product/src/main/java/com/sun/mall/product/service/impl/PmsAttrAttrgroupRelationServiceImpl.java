package com.sun.mall.product.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.sun.mall.product.common.ResultCode;
import com.sun.mall.product.entity.PmsAttrAttrgroupRelation;
import com.sun.mall.product.exception.BizRuntimeException;
import com.sun.mall.product.mapper.PmsAttrAttrgroupRelationMapper;
import com.sun.mall.product.service.PmsAttrAttrgroupRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 属性&属性分组关联 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class PmsAttrAttrgroupRelationServiceImpl extends ServiceImpl<PmsAttrAttrgroupRelationMapper, PmsAttrAttrgroupRelation> implements PmsAttrAttrgroupRelationService {


    @Override
    public boolean relateBatchAttrs(List<PmsAttrAttrgroupRelation> attrAttrgroupRelation) {
        List<Long> attrIds = attrAttrgroupRelation.stream().map(attrAttrgroupRelation1 -> attrAttrgroupRelation1.getAttrId()).collect(Collectors.toList());
        List<PmsAttrAttrgroupRelation> relatedAttrs = this.list(Wrappers.<PmsAttrAttrgroupRelation>lambdaQuery().in(PmsAttrAttrgroupRelation::getAttrId, attrIds));
        List<PmsAttrAttrgroupRelation> finalAttrAttrgroupRelation = attrAttrgroupRelation;
        relatedAttrs = relatedAttrs.stream().map(relatedAttr -> {
            relatedAttr.setAttrGroupId(finalAttrAttrgroupRelation.get(0).getAttrGroupId());
            return relatedAttr;
        }).collect(Collectors.toList());
        if (relatedAttrs != null && relatedAttrs.size() != 0){
            boolean result = this.updateBatchById(relatedAttrs);
            if(!result){
                throw BizRuntimeException.from(ResultCode.ERROR,"新增关联失败");
            }
        }
        List<PmsAttrAttrgroupRelation> finalRelatedAttrs = relatedAttrs;
        attrAttrgroupRelation = attrAttrgroupRelation.stream().filter(attrAttrgroupRelation1 -> {
            boolean flag = true;
            for (PmsAttrAttrgroupRelation relatedAttr : finalRelatedAttrs) {
                if(relatedAttr.getAttrId() == attrAttrgroupRelation1.getAttrId()){
                    flag = false;
                    break;
                }
            }
            return flag;
        }).collect(Collectors.toList());
        if (attrAttrgroupRelation != null && attrAttrgroupRelation.size() != 0){
            boolean result = this.saveBatch(attrAttrgroupRelation);
            if(!result){
                throw BizRuntimeException.from(ResultCode.ERROR,"新增关联失败");
            }
        }
        return true;
    }
}
