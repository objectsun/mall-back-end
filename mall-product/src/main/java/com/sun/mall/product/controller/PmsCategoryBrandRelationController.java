package com.sun.mall.product.controller;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.sun.mall.product.common.Result;
import com.sun.mall.product.entity.PmsBrand;
import com.sun.mall.product.entity.PmsCategoryBrandRelation;
import com.sun.mall.product.service.PmsCategoryBrandRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 品牌分类关联 前端控制器
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@RestController
@RequestMapping("/product/pms-category-brand-relation")
public class PmsCategoryBrandRelationController {
    @Autowired
    private PmsCategoryBrandRelationService categoryBrandRelationService;
    @GetMapping("/getCategoryBrandRelation")
    public Result<Object> getCategoryBrandRelation(Long brandId){
        List<PmsCategoryBrandRelation> categoryBrandRelations = categoryBrandRelationService.list(Wrappers.<PmsCategoryBrandRelation>lambdaQuery().eq(PmsCategoryBrandRelation::getBrandId, brandId));
        return Result.ok(categoryBrandRelations);
    }
    @GetMapping("/getBrandsByCatId")
    public Result<Object> getBrandsByCatId(Long catId){
        List<PmsBrand> brandList = categoryBrandRelationService.getBrandsByCatId(catId);
        return Result.ok(brandList);
    }
    @DeleteMapping("/deleteCateRelationHandle/{id}")
    public Result<Object> deleteCateRelationHandle(@PathVariable("id") Long id){
        boolean result = categoryBrandRelationService.removeById(id);
        return Result.ok(result);
    }
    @PostMapping ("/relateBrandWithCategory")
    public Result<Object> relateBrandWithCategory(@RequestBody PmsCategoryBrandRelation categoryBrandRelation){
        boolean result = categoryBrandRelationService.relateBrandWithCategory(categoryBrandRelation);
        return Result.ok();
    }
}

