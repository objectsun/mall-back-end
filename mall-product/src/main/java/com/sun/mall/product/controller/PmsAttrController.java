package com.sun.mall.product.controller;


import com.sun.mall.product.common.Result;
import com.sun.mall.product.entity.PmsAttr;
import com.sun.mall.product.entity.PmsAttrGroup;
import com.sun.mall.product.entity.PmsBrand;
import com.sun.mall.product.service.PmsAttrService;
import com.sun.mall.product.vo.AttrVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 商品属性 前端控制器
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@RestController
@RequestMapping("/product/pms-attr")
public class PmsAttrController {

    @Autowired
    private PmsAttrService attrService;

    @PostMapping("/saveAttr")
    public Result<Object> saveAttr(@RequestBody AttrVo attrVo){
        boolean result = attrService.saveAttr(attrVo);
        return Result.ok();
    }

    @PutMapping("/updateAttr")
    public Result<Object> updateAttr(@RequestBody AttrVo attrVo){
        boolean result = attrService.updateAttr(attrVo);
        return Result.ok();
    }
    @GetMapping("/list/{catId}/{type}")
    public Result<Object> list(@RequestParam String key,@PathVariable("catId") Long catId,@PathVariable("type")String type){
        List<AttrVo> attrList = attrService.listByCatId(type,key,catId);
        return Result.ok(attrList);
    }
    @GetMapping("/getAttr")
    public Result<Object> getAttr(Long attrId){
        AttrVo attr = attrService.getAttr(attrId);
        return Result.ok(attr);
    }

}

