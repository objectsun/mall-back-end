package com.sun.mall.product.controller;


import com.sun.mall.product.common.Result;
import com.sun.mall.product.entity.PmsSkuInfo;
import com.sun.mall.product.entity.PmsSpuInfo;
import com.sun.mall.product.query.SkuInfoQuery;
import com.sun.mall.product.query.SpuInfoQuery;
import com.sun.mall.product.service.PmsSkuInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * sku信息 前端控制器
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@RestController
@RequestMapping("/product/pms-sku-info")
public class PmsSkuInfoController {

    @Autowired
    private PmsSkuInfoService skuInfoService;
    @PostMapping("/getSkuInfos")
    public Result<Object> getSkuInfos(@RequestBody SkuInfoQuery query){
        List<PmsSkuInfo> spuInfos = skuInfoService.getSkuInfos(query);
        return Result.ok(spuInfos);
    }
}

