package com.sun.mall.product.service;

import com.sun.mall.product.entity.PmsAttr;
import com.sun.mall.product.entity.PmsAttrAttrgroupRelation;
import com.sun.mall.product.entity.PmsAttrGroup;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sun.mall.product.vo.AttrGroupWithAttrsVo;

import java.util.List;

/**
 * <p>
 * 属性分组 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface PmsAttrGroupService extends IService<PmsAttrGroup> {

    List<PmsAttrGroup> queryAttrGroupBycatelogId(String key, Long catelogId);

    List<PmsAttr> getRelatedAttributions(Long attrGroupId);

    boolean deleteRelationWithAttr(Long attrId,Long attrGroupId);

    List<PmsAttr> getEnabledRelationAttr(Long attrGroupId, String key);

    boolean deleteBatchRelationWithAttr(List<PmsAttrAttrgroupRelation> attrAttrgroupRelations);

    List<AttrGroupWithAttrsVo> attrGroupWithAttr(Long catId);
}
