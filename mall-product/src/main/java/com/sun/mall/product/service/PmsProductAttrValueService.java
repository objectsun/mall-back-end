package com.sun.mall.product.service;

import com.sun.mall.product.entity.PmsProductAttrValue;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * spu属性值 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface PmsProductAttrValueService extends IService<PmsProductAttrValue> {

}
