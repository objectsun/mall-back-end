package com.sun.mall.product.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.sun.mall.product.common.ResultCode;
import com.sun.mall.product.dto.CategoryDTO;
import com.sun.mall.product.entity.PmsCategory;
import com.sun.mall.product.entity.PmsCategoryBrandRelation;
import com.sun.mall.product.exception.BizRuntimeException;
import com.sun.mall.product.mapper.PmsCategoryMapper;
import com.sun.mall.product.service.PmsCategoryBrandRelationService;
import com.sun.mall.product.service.PmsCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 商品三级分类 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class PmsCategoryServiceImpl extends ServiceImpl<PmsCategoryMapper, PmsCategory> implements PmsCategoryService {

    @Autowired
    private PmsCategoryBrandRelationService categoryBrandRelationService;

    @Override
    public List<CategoryDTO> listWithTree() {
        List<PmsCategory> pmsCategories = this.list();
        List<CategoryDTO> categoryDTOS = pmsCategories.stream().map(category -> {
            CategoryDTO categoryDTO = new CategoryDTO();
            BeanUtil.copyProperties(category,categoryDTO);
            return categoryDTO;
        }).collect(Collectors.toList());

        List<CategoryDTO> result = categoryDTOS.stream().map(category -> {
            if (category.getCatLevel() != 3){
                category.setChildren(getChildren(category,categoryDTOS));
            }
            return category;
        }).filter(categoryDTO -> {
            if (categoryDTO.getParentCid() == 0){
                return true;
            }
            return false;
        }).collect(Collectors.toList());

//        List<CategoryDTO> result = categoryDTOS.stream().map(category -> {
//            if (category.getCatId() != 0){
//                category.setParentCategory(getParentCategory(category,categoryDTOS));
//            }
//            return category;
//        }).collect(Collectors.toList());
        result.sort(Comparator.comparingInt(CategoryDTO::getSort));
        return result;
    }

    @Override
    @Transactional
    public boolean deleteCategories(Long[] ids) {
        //TODO 判断改值是否可以删除
        boolean result = this.removeBatchByIds(Arrays.asList(ids));
        return true;
    }

    @Override
    public List<Long> getCategoryWithParent(Long catId) {
        List<PmsCategory> categories = this.list();
        PmsCategory category = this.getById(catId);
        List<Long> result = new ArrayList<>();
        getCategoryIdWithParentId(categories,category,result);
        return result;
    }

    @Override
    public boolean editCategory(PmsCategory category) {
        boolean result = this.updateById(category);
        if (!result){
            throw BizRuntimeException.from(ResultCode.ERROR,"修改品牌失败");
        }
        List<PmsCategoryBrandRelation> pmsCategoryBrandRelations = categoryBrandRelationService.list(Wrappers.<PmsCategoryBrandRelation>lambdaQuery().eq(PmsCategoryBrandRelation::getCatelogId, category.getCatId()));
        pmsCategoryBrandRelations = pmsCategoryBrandRelations.stream().map(pmsCategoryBrandRelation -> {
            pmsCategoryBrandRelation.setCatelogName(category.getName());
            return pmsCategoryBrandRelation;
        }).collect(Collectors.toList());
        result = categoryBrandRelationService.updateBatchById(pmsCategoryBrandRelations);
        if (!result){
            throw BizRuntimeException.from(ResultCode.ERROR,"修改品牌失败");
        }
        return true;
    }

    private void getCategoryIdWithParentId(List<PmsCategory> categories, PmsCategory NowCategory, List<Long> result) {
        if (NowCategory.getParentCid() == 0){
            result.add(NowCategory.getCatId());
            return;
        }
        categories.forEach(category -> {
            if (NowCategory.getParentCid() == category.getCatId()){
                getCategoryIdWithParentId(categories,category,result);
                result.add(NowCategory.getCatId());
            }
        });
    }

    private List<CategoryDTO> getChildren(CategoryDTO category, List<CategoryDTO> categoryDTOS) {
        if(category.getCatLevel() == 3){
            return null;
        }
        List<CategoryDTO> children = new ArrayList<>();
        categoryDTOS.forEach(comparedCategory -> {
            if (comparedCategory.getParentCid() == category.getCatId()){
                children.add(comparedCategory);
                comparedCategory.setChildren(getChildren(comparedCategory,categoryDTOS));
            }
        });
        if (children.size() != 0){
            children.sort(Comparator.comparingInt(CategoryDTO::getSort));
        }
        category.setChildren(children);
        return category.getChildren();
    }

    private CategoryDTO getParentCategory(CategoryDTO category, List<CategoryDTO> categoryDTOS) {
        if(category.getCatId() == 0){
            return null;
        }
        categoryDTOS.forEach(comparedCategory -> {
            if (comparedCategory.getCatId() == category.getParentCid()){
                comparedCategory.setParentCategory(getParentCategory(comparedCategory,categoryDTOS));
                category.setParentCategory(comparedCategory);
            }
        });
        return category.getParentCategory();
    }

}
