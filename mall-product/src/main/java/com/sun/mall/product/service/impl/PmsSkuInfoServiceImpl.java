package com.sun.mall.product.service.impl;

import com.sun.mall.product.entity.PmsSkuInfo;
import com.sun.mall.product.entity.PmsSpuInfo;
import com.sun.mall.product.mapper.PmsSkuInfoMapper;
import com.sun.mall.product.query.SkuInfoQuery;
import com.sun.mall.product.service.PmsSkuInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * sku信息 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class PmsSkuInfoServiceImpl extends ServiceImpl<PmsSkuInfoMapper, PmsSkuInfo> implements PmsSkuInfoService {

    @Override
    public List<PmsSkuInfo> getSkuInfos(SkuInfoQuery query) {
        List<PmsSkuInfo> skuInfos = this.list();
        if (query == null){
            return skuInfos;
        }
        skuInfos = skuInfos.stream().filter(skuInfo -> {
            boolean flag = true;
            if(query.getBrandId() != 0){
                flag = flag && skuInfo.getBrandId().longValue() == query.getBrandId().longValue();
            }
            if(query.getCatelogId() != 0){
                flag = flag && skuInfo.getCatalogId().longValue() == query.getCatelogId().longValue();
            }
            if(query.getMin() != null && query.getMax() != null && (query.getMin().longValue() < query.getMax().longValue())){
                flag = flag && skuInfo.getPrice().longValue() > query.getMin().longValue()
                        && skuInfo.getPrice().longValue() < query.getMax().longValue();
            }
            return flag;
        }).collect(Collectors.toList());
        return skuInfos;
    }
}
