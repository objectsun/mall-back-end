package com.sun.mall.product.service.impl;

import com.sun.mall.product.entity.PmsSpuComment;
import com.sun.mall.product.mapper.PmsSpuCommentMapper;
import com.sun.mall.product.service.PmsSpuCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品评价 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class PmsSpuCommentServiceImpl extends ServiceImpl<PmsSpuCommentMapper, PmsSpuComment> implements PmsSpuCommentService {

}
