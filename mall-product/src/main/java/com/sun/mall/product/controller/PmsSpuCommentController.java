package com.sun.mall.product.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 商品评价 前端控制器
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@RestController
@RequestMapping("/product/pms-spu-comment")
public class PmsSpuCommentController {

}

