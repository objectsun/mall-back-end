package com.sun.mall.product.mapper;

import com.sun.mall.product.entity.PmsCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品三级分类 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface PmsCategoryMapper extends BaseMapper<PmsCategory> {

}
