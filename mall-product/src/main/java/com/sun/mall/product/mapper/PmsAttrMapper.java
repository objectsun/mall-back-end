package com.sun.mall.product.mapper;

import com.sun.mall.product.entity.PmsAttr;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sun.mall.product.vo.AttrVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 商品属性 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface PmsAttrMapper extends BaseMapper<PmsAttr> {

    List<AttrVo> listByCatId(@Param("attrType") int attrType,@Param("key") String key,@Param("catId") Long catId);
}
