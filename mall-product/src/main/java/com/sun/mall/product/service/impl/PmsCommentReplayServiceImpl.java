package com.sun.mall.product.service.impl;

import com.sun.mall.product.entity.PmsCommentReplay;
import com.sun.mall.product.mapper.PmsCommentReplayMapper;
import com.sun.mall.product.service.PmsCommentReplayService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品评价回复关系 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class PmsCommentReplayServiceImpl extends ServiceImpl<PmsCommentReplayMapper, PmsCommentReplay> implements PmsCommentReplayService {

}
