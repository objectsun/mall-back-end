package com.sun.mall.product.service;

import com.sun.mall.product.entity.PmsSkuSaleAttrValue;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * sku销售属性&值 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface PmsSkuSaleAttrValueService extends IService<PmsSkuSaleAttrValue> {

}
