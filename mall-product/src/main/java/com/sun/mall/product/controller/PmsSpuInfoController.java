package com.sun.mall.product.controller;


import com.sun.mall.product.common.Result;
import com.sun.mall.product.entity.PmsCategory;
import com.sun.mall.product.entity.PmsSpuInfo;
import com.sun.mall.product.query.SpuInfoQuery;
import com.sun.mall.product.service.PmsSpuInfoService;
import com.sun.mall.product.vo.spu.SpuSaveVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Action;
import java.util.List;

/**
 * <p>
 * spu信息 前端控制器
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@RestController
@RequestMapping("/product/pms-spu-info")
public class PmsSpuInfoController {

    @Autowired
    private PmsSpuInfoService spuInfoService;

    @PostMapping("/saveSpuInfo")
    public Result<Object> saveSpuInfo(@RequestBody SpuSaveVo vo){
        boolean result = spuInfoService.saveSpuInfo(vo);
        return Result.ok();
    }

    @PostMapping("/getSpuInfos")
    public Result<Object> getSpuInfos(@RequestBody SpuInfoQuery query){
        List<PmsSpuInfo> spuInfos = spuInfoService.getSpuInfos(query);
        return Result.ok(spuInfos);
    }
}

