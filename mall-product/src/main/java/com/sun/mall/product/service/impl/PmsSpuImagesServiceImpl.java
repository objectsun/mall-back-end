package com.sun.mall.product.service.impl;

import com.sun.mall.product.entity.PmsSpuImages;
import com.sun.mall.product.mapper.PmsSpuImagesMapper;
import com.sun.mall.product.service.PmsSpuImagesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * spu图片 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class PmsSpuImagesServiceImpl extends ServiceImpl<PmsSpuImagesMapper, PmsSpuImages> implements PmsSpuImagesService {

    @Override
    public void saveImages(Long id, List<String> images) {
        if(images == null || images.size() == 0){
            return;
        }
        List<PmsSpuImages> collect = images.stream().map(image -> {
            PmsSpuImages spuImages = new PmsSpuImages();
            spuImages.setSpuId(id);
            spuImages.setImgUrl(image);
            return spuImages;
        }).collect(Collectors.toList());

        this.saveBatch(collect);
    }
}
