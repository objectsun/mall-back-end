package com.sun.mall.product.mapper;

import com.sun.mall.product.entity.PmsCategoryBrandRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 品牌分类关联 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface PmsCategoryBrandRelationMapper extends BaseMapper<PmsCategoryBrandRelation> {

}
