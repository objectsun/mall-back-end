package com.sun.mall.product.service;

import com.sun.mall.product.entity.PmsBrand;
import com.sun.mall.product.entity.PmsCategoryBrandRelation;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 品牌分类关联 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface PmsCategoryBrandRelationService extends IService<PmsCategoryBrandRelation> {

    boolean relateBrandWithCategory(PmsCategoryBrandRelation categoryBrandRelation);

    List<PmsBrand> getBrandsByCatId(Long catId);
}
