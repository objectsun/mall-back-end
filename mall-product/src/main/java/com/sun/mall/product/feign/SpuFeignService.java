package com.sun.mall.product.feign;

import com.sun.mall.product.common.Result;
import com.sun.mall.product.to.SkuReductionTo;
import com.sun.mall.product.to.SpuBoundTo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@FeignClient("mall-coupon")
public interface SpuFeignService {
    @PostMapping("/coupon/sms-spu-bounds/saveSpuBounds")
    Result<Object> saveSpuBounds(@RequestBody SpuBoundTo spuBoundTo);
    @PostMapping("/coupon/sms-sku-full-reduction/saveSkuReduction")
    Result<Object> saveSkuReduction(@RequestBody SkuReductionTo skuReductionTo);
}
