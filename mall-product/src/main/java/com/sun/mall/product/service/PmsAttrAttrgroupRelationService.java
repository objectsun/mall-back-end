package com.sun.mall.product.service;

import com.sun.mall.product.entity.PmsAttrAttrgroupRelation;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 属性&属性分组关联 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface PmsAttrAttrgroupRelationService extends IService<PmsAttrAttrgroupRelation> {

    boolean relateBatchAttrs(List<PmsAttrAttrgroupRelation> attrAttrgroupRelation);
}
