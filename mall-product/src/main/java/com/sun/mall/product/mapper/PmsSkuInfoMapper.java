package com.sun.mall.product.mapper;

import com.sun.mall.product.entity.PmsSkuInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * sku信息 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface PmsSkuInfoMapper extends BaseMapper<PmsSkuInfo> {

}
