package com.sun.mall.product.service;

import com.sun.mall.product.entity.PmsSpuInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sun.mall.product.query.SpuInfoQuery;
import com.sun.mall.product.vo.spu.SpuSaveVo;

import java.util.List;

/**
 * <p>
 * spu信息 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface PmsSpuInfoService extends IService<PmsSpuInfo> {

    boolean saveSpuInfo(SpuSaveVo vo);

    void saveBaseSpuInfo(PmsSpuInfo spuInfo);

    List<PmsSpuInfo> getSpuInfos(SpuInfoQuery query);
}
