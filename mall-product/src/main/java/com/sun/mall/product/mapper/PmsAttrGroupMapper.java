package com.sun.mall.product.mapper;

import com.sun.mall.product.entity.PmsAttrGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 属性分组 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface PmsAttrGroupMapper extends BaseMapper<PmsAttrGroup> {

}
