package com.sun.mall.product.service;

import com.sun.mall.product.entity.PmsCommentReplay;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品评价回复关系 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface PmsCommentReplayService extends IService<PmsCommentReplay> {

}
