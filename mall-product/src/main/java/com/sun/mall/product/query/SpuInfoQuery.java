package com.sun.mall.product.query;

import lombok.Data;

@Data
public class SpuInfoQuery {
    private Long catelogId;

    private Long brandId;

    private Integer publishStatus;

//    在后面es涉及到
    private String key;
}
