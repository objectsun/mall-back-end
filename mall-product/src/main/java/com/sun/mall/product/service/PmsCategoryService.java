package com.sun.mall.product.service;

import com.sun.mall.product.dto.CategoryDTO;
import com.sun.mall.product.entity.PmsCategory;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 商品三级分类 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface PmsCategoryService extends IService<PmsCategory> {

    List<CategoryDTO> listWithTree();

    boolean deleteCategories(Long[] ids);

    List<Long> getCategoryWithParent(Long catId);

    boolean editCategory(PmsCategory category);
}
