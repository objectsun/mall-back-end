package com.sun.mall.product.service;

import com.sun.mall.product.entity.PmsSpuComment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品评价 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface PmsSpuCommentService extends IService<PmsSpuComment> {

}
