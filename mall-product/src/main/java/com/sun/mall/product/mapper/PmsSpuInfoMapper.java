package com.sun.mall.product.mapper;

import com.sun.mall.product.entity.PmsSpuInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * spu信息 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface PmsSpuInfoMapper extends BaseMapper<PmsSpuInfo> {

}
