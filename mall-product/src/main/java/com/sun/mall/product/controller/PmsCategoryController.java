package com.sun.mall.product.controller;


import com.sun.mall.product.common.Result;
import com.sun.mall.product.dto.CategoryDTO;
import com.sun.mall.product.entity.PmsCategory;
import com.sun.mall.product.service.PmsCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.RequestWrapper;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 商品三级分类 前端控制器
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@RestController
@RequestMapping("/product/pms-category")
public class PmsCategoryController {
    @Autowired
    private PmsCategoryService categoryService;

    @GetMapping("/list/tree")
    public Result<Object> list(){
        List<CategoryDTO> categories = categoryService.listWithTree();
        return Result.ok(categories);
    }
    @PostMapping("/delete")
    public Result<Object> deleteCategories(@RequestBody Long[] ids){
        boolean result = categoryService.deleteCategories(ids);
        return Result.ok();
    }
    @PostMapping("/append")
    public Result<Object> appendCategory(@RequestBody PmsCategory category){
        boolean result = categoryService.save(category);
        return Result.ok();
    }
    @PutMapping("/edit")
    public Result<Object> editCategory(@RequestBody PmsCategory category){
        boolean result = categoryService.editCategory(category);
        return Result.ok();
    }
    @PutMapping("/update/sort")
    public Result<Object> updateSort(@RequestBody List<PmsCategory> categories){
        boolean result = categoryService.updateBatchById(categories);
        return Result.ok();
    }
    @GetMapping("/getCategoryWithParent")
    public Result<Object> getCategoryWithParent(Long catId){
        List<Long> result = new ArrayList<>();
        result = categoryService.getCategoryWithParent(catId);
        return Result.ok(result);
    }
}

