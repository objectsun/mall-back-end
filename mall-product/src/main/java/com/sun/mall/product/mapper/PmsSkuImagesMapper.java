package com.sun.mall.product.mapper;

import com.sun.mall.product.entity.PmsSkuImages;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * sku图片 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface PmsSkuImagesMapper extends BaseMapper<PmsSkuImages> {

}
