package com.sun.mall.product.service;

import com.sun.mall.product.entity.PmsSpuImages;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * spu图片 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface PmsSpuImagesService extends IService<PmsSpuImages> {

    void saveImages(Long id, List<String> images);
}
