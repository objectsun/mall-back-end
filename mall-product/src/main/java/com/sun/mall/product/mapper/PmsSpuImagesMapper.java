package com.sun.mall.product.mapper;

import com.sun.mall.product.entity.PmsSpuImages;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * spu图片 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface PmsSpuImagesMapper extends BaseMapper<PmsSpuImages> {

}
