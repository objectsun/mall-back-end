package com.sun.mall.product.service.impl;

import com.sun.mall.product.entity.PmsSkuImages;
import com.sun.mall.product.mapper.PmsSkuImagesMapper;
import com.sun.mall.product.service.PmsSkuImagesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * sku图片 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class PmsSkuImagesServiceImpl extends ServiceImpl<PmsSkuImagesMapper, PmsSkuImages> implements PmsSkuImagesService {

}
