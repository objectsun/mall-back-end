package com.sun.mall.product.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sun.mall.product.common.Result;
import com.sun.mall.product.dto.CategoryDTO;
import com.sun.mall.product.entity.PmsBrand;
import com.sun.mall.product.service.PmsBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 品牌 前端控制器
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@RestController
@RequestMapping("/product/pms-brand")
public class PmsBrandController {
    @Autowired
    private PmsBrandService brandService;

    @GetMapping("/list")
    public Result<Object> queryBrandsForPage(String key){
        List<PmsBrand> brandList = brandService.queryBrandsForPage(key);
        return Result.ok(brandList);
    }
    @GetMapping("/getBrand")
    public Result<Object> getBrand(String brandId){
        PmsBrand brand = brandService.getById(brandId);
        return Result.ok(brand);
    }
    @PutMapping("/updateBrand")
    public Result<Object> updateBrand(@RequestBody PmsBrand brand){
        boolean result = brandService.updateBrand(brand);
        return Result.ok();
    }
    @PostMapping("/saveBrand")
    public Result<Object> saveBrand(@RequestBody PmsBrand brand){
        boolean result = brandService.save(brand);
        return Result.ok();
    }
    @PutMapping("/updateBrandStatus")
    public Result<Object> updateBrandStatus(@RequestBody PmsBrand brand){
        boolean result = brandService.updateById(brand);
        return Result.ok();
    }
    @PostMapping("/delete")
    public Result<Object> deleteBrands(@RequestBody List<Long> ids){
        boolean result = brandService.removeBatchByIds(ids);
        return Result.ok();
    }
}

