package com.sun.mall.product.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.sun.mall.product.entity.PmsBrand;
import com.sun.mall.product.entity.PmsCategory;
import com.sun.mall.product.entity.PmsCategoryBrandRelation;
import com.sun.mall.product.mapper.PmsBrandMapper;
import com.sun.mall.product.mapper.PmsCategoryBrandRelationMapper;
import com.sun.mall.product.mapper.PmsCategoryMapper;
import com.sun.mall.product.service.PmsBrandService;
import com.sun.mall.product.service.PmsCategoryBrandRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 品牌分类关联 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class PmsCategoryBrandRelationServiceImpl extends ServiceImpl<PmsCategoryBrandRelationMapper, PmsCategoryBrandRelation> implements PmsCategoryBrandRelationService {
    @Autowired
    private PmsBrandMapper brandMapper;

    @Autowired
    private PmsCategoryMapper categoryMapper;

    @Override
    public boolean relateBrandWithCategory(PmsCategoryBrandRelation categoryBrandRelation) {
        Long brandId = categoryBrandRelation.getBrandId();
        Long catelogId = categoryBrandRelation.getCatelogId();
        PmsBrand brand = brandMapper.selectById(brandId);
        PmsCategory category = categoryMapper.selectById(catelogId);
        categoryBrandRelation.setBrandName(brand.getName());
        categoryBrandRelation.setCatelogName(category.getName());
        boolean result = this.save(categoryBrandRelation);
        return result;
    }

    @Override
    public List<PmsBrand> getBrandsByCatId(Long catId) {
        List<PmsCategoryBrandRelation> categoryBrandRelations = this.list(Wrappers.<PmsCategoryBrandRelation>lambdaQuery().eq(PmsCategoryBrandRelation::getCatelogId, catId));
        if(categoryBrandRelations == null || categoryBrandRelations.size() == 0){
            return new ArrayList<PmsBrand>();
        }
        List<Long> brandIds = categoryBrandRelations.stream().map(categoryBrandRelation -> categoryBrandRelation.getBrandId()).collect(Collectors.toList());
        List<PmsBrand> brandList = brandMapper.selectBatchIds(brandIds);
        return brandList;
    }
}
