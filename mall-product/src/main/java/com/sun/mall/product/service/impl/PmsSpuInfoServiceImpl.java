package com.sun.mall.product.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.sun.mall.product.entity.*;
import com.sun.mall.product.feign.SpuFeignService;
import com.sun.mall.product.mapper.PmsSpuInfoMapper;
import com.sun.mall.product.query.SpuInfoQuery;
import com.sun.mall.product.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sun.mall.product.to.SkuReductionTo;
import com.sun.mall.product.to.SpuBoundTo;
import com.sun.mall.product.vo.spu.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * spu信息 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class PmsSpuInfoServiceImpl extends ServiceImpl<PmsSpuInfoMapper, PmsSpuInfo> implements PmsSpuInfoService {

    @Autowired
    private PmsSpuInfoDescService spuInfoDescService;
    @Autowired
    private PmsSpuImagesService spuImagesService;
    @Autowired
    private PmsAttrService attrService;
    @Autowired
    private PmsSkuInfoService skuInfoService;
    @Autowired
    private PmsProductAttrValueService productAttrValueService;
    @Autowired
    private PmsSkuImagesService skuImagesService;
    @Autowired
    private PmsSkuSaleAttrValueService skuSaleAttrValueService;
    @Autowired
    private SpuFeignService spuFeignService;
    @Override
    @Transactional
    public boolean saveSpuInfo(SpuSaveVo vo) {
        //1、保存spu基本信息 pms_spu_info
        PmsSpuInfo spuInfo = new PmsSpuInfo();
        BeanUtil.copyProperties(vo,spuInfo);
        spuInfo.setCreateTime(new Date());
        spuInfo.setUpdateTime(new Date());
        this.saveBaseSpuInfo(spuInfo);

        //2、保存Spu描述图片 pms_spu_info_desc
        List<String> decript = vo.getDecript();
        PmsSpuInfoDesc spuInfoDesc = new PmsSpuInfoDesc();
        spuInfoDesc.setSpuId(spuInfo.getId());
        spuInfoDesc.setDecript(String.join(",",decript));
        spuInfoDescService.saveSpuInfoDesc(spuInfoDesc);

        //3、保存spu的图片集 pms_spu_images
        List<String> images = vo.getImages();
        spuImagesService.saveImages(spuInfo.getId(),images);

        //4、保存spu的规格参数；pms_product_attr_value
        List<BaseAttrs> baseAttrs = vo.getBaseAttrs();
        List<PmsProductAttrValue> collect = baseAttrs.stream().map(attr -> {
            PmsProductAttrValue productAttrValue = new PmsProductAttrValue();
            productAttrValue.setAttrId(attr.getAttrId());

            PmsAttr pmsAttr = attrService.getById(attr.getAttrId());
            productAttrValue.setAttrName(pmsAttr.getAttrName());
            productAttrValue.setAttrValue(attr.getAttrValues());
            productAttrValue.setQuickShow(attr.getShowDesc());
            productAttrValue.setSpuId(spuInfo.getId());
            return productAttrValue;
        }).collect(Collectors.toList());
        productAttrValueService.saveBatch(collect);

        //5、保存spu的积分信息；mall_sms->sms_spu_bounds
        Bounds bounds = vo.getBounds();
        SpuBoundTo spuBoundTo = new SpuBoundTo();
        BeanUtil.copyProperties(bounds,spuBoundTo);
        spuBoundTo.setSpuId(spuInfo.getId());
        spuFeignService.saveSpuBounds(spuBoundTo);

        //6、保存当前spu对应的所有sku信息
        //6.1、sku的基本信息；pms_sku_info
        List<Skus> skus = vo.getSkus();
        if(skus != null && skus.size() != 0){
            skus.forEach(item -> {
                String defaultImg = "";
                for (Images image : item.getImages()) {
                    if(image.getDefaultImg() == 1){
                        defaultImg = image.getImgUrl();
                    }
                }
                PmsSkuInfo skuInfo = new PmsSkuInfo();
                BeanUtil.copyProperties(item,skuInfo);
                skuInfo.setBrandId(spuInfo.getBrandId());
                skuInfo.setCatalogId(spuInfo.getCatalogId());
                skuInfo.setSaleCount(0L);
                skuInfo.setSpuId(spuInfo.getId());
                skuInfo.setSkuDefaultImg(defaultImg);
                skuInfoService.save(skuInfo);

                Long skuId = skuInfo.getSkuId();
                List<PmsSkuImages> imagesList = item.getImages().stream().map(img -> {
                    PmsSkuImages skuImages = new PmsSkuImages();
                    skuImages.setSkuId(skuId);
                    skuImages.setImgUrl(img.getImgUrl());
                    skuImages.setDefaultImg(img.getDefaultImg());
                    return skuImages;
                }).filter(entity -> {
                    return StrUtil.isNotBlank(entity.getImgUrl());
                }).collect(Collectors.toList());
                //6.2、sku的图片信息；pms_sku_images
                skuImagesService.saveBatch(imagesList);
                //6.3、sku的销售属性信息；pms_sku_sale_attr_value
                List<Attr> attr = item.getAttr();
                List<PmsSkuSaleAttrValue> skuSaleAttrValues = attr.stream().map(a -> {
                    PmsSkuSaleAttrValue skuSaleAttrValue = new PmsSkuSaleAttrValue();
                    BeanUtil.copyProperties(a, skuSaleAttrValue);
                    skuSaleAttrValue.setSkuId(skuId);
                    return skuSaleAttrValue;
                }).collect(Collectors.toList());
                skuSaleAttrValueService.saveBatch(skuSaleAttrValues);
                //6.4、sku的优惠、满减等信息；mall_sms->sms_sku_ladder\sms_sku_full_reduction
                SkuReductionTo skuReductionTo = new SkuReductionTo();
                BeanUtil.copyProperties(item,skuReductionTo);
                skuReductionTo.setSkuId(skuInfo.getSkuId());
                spuFeignService.saveSkuReduction(skuReductionTo);
            });

        }

        return true;
    }

    @Override
    public void saveBaseSpuInfo(PmsSpuInfo spuInfo) {
        this.save(spuInfo);
    }

    @Override
    public List<PmsSpuInfo> getSpuInfos(SpuInfoQuery query) {
        List<PmsSpuInfo> spuInfos = this.list();
        spuInfos = spuInfos.stream().filter(spuInfo -> {
            boolean flag = true;
            if(query != null && query.getBrandId() != null && query.getBrandId() != 0){
                flag = flag && (query.getBrandId().longValue() == spuInfo.getBrandId().longValue());
            }
            if(query != null && query.getCatelogId() != null && query.getCatelogId() != 0){
                flag = flag && (query.getCatelogId().longValue() == spuInfo.getCatalogId().longValue());
            }
            if(query != null && query.getPublishStatus() != null){
                flag = flag && (query.getPublishStatus().longValue() == spuInfo.getPublishStatus().longValue());
            }
            return flag;
        }).collect(Collectors.toList());
        return spuInfos;
    }
}
