package com.sun.mall.product.service;

import com.sun.mall.product.entity.PmsSkuImages;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * sku图片 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface PmsSkuImagesService extends IService<PmsSkuImages> {

}
