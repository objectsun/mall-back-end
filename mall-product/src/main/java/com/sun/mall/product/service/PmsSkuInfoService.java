package com.sun.mall.product.service;

import com.sun.mall.product.entity.PmsSkuInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sun.mall.product.entity.PmsSpuInfo;
import com.sun.mall.product.query.SkuInfoQuery;

import java.util.List;

/**
 * <p>
 * sku信息 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface PmsSkuInfoService extends IService<PmsSkuInfo> {

    List<PmsSkuInfo> getSkuInfos(SkuInfoQuery query);
}
