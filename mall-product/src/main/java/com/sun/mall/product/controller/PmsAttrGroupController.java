package com.sun.mall.product.controller;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.sun.mall.product.common.Result;
import com.sun.mall.product.entity.PmsAttr;
import com.sun.mall.product.entity.PmsAttrAttrgroupRelation;
import com.sun.mall.product.entity.PmsAttrGroup;
import com.sun.mall.product.entity.PmsBrand;
import com.sun.mall.product.service.PmsAttrAttrgroupRelationService;
import com.sun.mall.product.service.PmsAttrGroupService;
import com.sun.mall.product.vo.AttrGroupWithAttrsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 属性分组 前端控制器
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@RestController
@RequestMapping("/product/pms-attr-group")
public class PmsAttrGroupController {
    @Autowired
    private PmsAttrGroupService attrGroupService;
    @Autowired
    private PmsAttrAttrgroupRelationService attrAttrgroupRelationService;

    @GetMapping("/list/{catelogId}")
    public Result<Object> queryAttrGroupByCatelogId(@RequestParam String key,
                               @PathVariable("catelogId") Long catelogId){
        List<PmsAttrGroup> attrGroups = attrGroupService.queryAttrGroupBycatelogId(key,catelogId);
        return Result.ok(attrGroups);
    }
    @GetMapping("/getAttrGroup")
    public Result<Object> getAttrGroup(Long attrGroupId){
        PmsAttrGroup attrGroup = attrGroupService.getById(attrGroupId);
        return Result.ok(attrGroup);
    }
    @GetMapping("/getEnabledRelationAttr")
    public Result<Object> getEnabledRelationAttr(Long attrGroupId,String key){
        List<PmsAttr> attrs = attrGroupService.getEnabledRelationAttr(attrGroupId,key);
        return Result.ok(attrs);
    }
    @GetMapping("/getRelatedAttributions")
    public Result<Object> getRelatedAttributions(Long attrGroupId){
        List<PmsAttr> attrs = attrGroupService.getRelatedAttributions(attrGroupId);
        return Result.ok(attrs);
    }
    @PostMapping("/saveAttrGroup")
    public Result<Object> saveAttrGroup(@RequestBody PmsAttrGroup attrGroup){
        boolean result = attrGroupService.save(attrGroup);
        return Result.ok();
    }
    @PostMapping("/relateBatchAttrs")
    public Result<Object> relateBatchAttrs(@RequestBody List<PmsAttrAttrgroupRelation> attrAttrgroupRelation){
        boolean result = attrAttrgroupRelationService.relateBatchAttrs(attrAttrgroupRelation);
        return Result.ok();
    }
    @PostMapping("/deleteAttrGroups")
    public Result<Object> deleteAttrGroups(@RequestBody List<Long> ids){
        boolean result = attrGroupService.removeBatchByIds(ids);
        return Result.ok();
    }
    @PutMapping("/updateAttrGroup")
    public Result<Object> updateAttrGroup(@RequestBody PmsAttrGroup attrGroup){
        boolean result = attrGroupService.updateById(attrGroup);
        return Result.ok();
    }
    @DeleteMapping("/deleteRelationWithAttr/{attrId}/{attrGroupId}")
    public Result<Object> deleteRelationWithAttr(@PathVariable("attrId") Long attrId,@PathVariable("attrGroupId") Long attrGroupId){
        boolean result = attrGroupService.deleteRelationWithAttr(attrId,attrGroupId);
        return Result.ok();
    }
    @PostMapping("/deleteBatchRelationWithAttr")
    public Result<Object> deleteBatchRelationWithAttr(@RequestBody List<PmsAttrAttrgroupRelation> attrAttrgroupRelations){
        boolean result = attrGroupService.deleteBatchRelationWithAttr(attrAttrgroupRelations);
        return Result.ok();
    }
    @GetMapping("/attrGroupWithAttr")
    public Result<Object> attrGroupWithAttr(Long catId){
        List<AttrGroupWithAttrsVo> attrGroupWithAttrs = attrGroupService.attrGroupWithAttr(catId);
        return Result.ok(attrGroupWithAttrs);
    }
}

