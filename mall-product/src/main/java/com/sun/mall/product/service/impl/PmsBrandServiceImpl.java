package com.sun.mall.product.service.impl;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.sun.mall.product.common.Result;
import com.sun.mall.product.common.ResultCode;
import com.sun.mall.product.entity.PmsBrand;
import com.sun.mall.product.entity.PmsCategoryBrandRelation;
import com.sun.mall.product.exception.BizRuntimeException;
import com.sun.mall.product.mapper.PmsBrandMapper;
import com.sun.mall.product.mapper.PmsCategoryBrandRelationMapper;
import com.sun.mall.product.service.PmsBrandService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sun.mall.product.service.PmsCategoryBrandRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 品牌 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class PmsBrandServiceImpl extends ServiceImpl<PmsBrandMapper, PmsBrand> implements PmsBrandService {

    @Autowired
    private PmsCategoryBrandRelationService categoryBrandRelationService;
    @Override
    public List<PmsBrand> queryBrandsForPage(String key) {
        if (StringUtils.isBlank(key)){
            List<PmsBrand> brandList = this.list();
            return brandList;
        }
        List<PmsBrand> brandList = this.list(Wrappers.<PmsBrand>lambdaQuery().like(PmsBrand::getName,key).or().like(PmsBrand::getBrandId,key));
        return brandList;
    }

    @Override
    @Transactional
    public boolean updateBrand(PmsBrand brand) {
        boolean result = this.updateById(brand);
        if (!result){
            throw BizRuntimeException.from(ResultCode.ERROR,"修改品牌失败");
        }
        List<PmsCategoryBrandRelation> pmsCategoryBrandRelations = categoryBrandRelationService.list(Wrappers.<PmsCategoryBrandRelation>lambdaQuery().eq(PmsCategoryBrandRelation::getBrandId, brand.getBrandId()));
        pmsCategoryBrandRelations = pmsCategoryBrandRelations.stream().map(pmsCategoryBrandRelation -> {
            pmsCategoryBrandRelation.setBrandName(brand.getName());
            return pmsCategoryBrandRelation;
        }).collect(Collectors.toList());
        result = categoryBrandRelationService.updateBatchById(pmsCategoryBrandRelations);
        if (!result){
            throw BizRuntimeException.from(ResultCode.ERROR,"修改品牌失败");
        }
        return true;
    }
}
