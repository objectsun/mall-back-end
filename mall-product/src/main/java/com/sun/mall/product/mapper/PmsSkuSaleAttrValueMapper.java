package com.sun.mall.product.mapper;

import com.sun.mall.product.entity.PmsSkuSaleAttrValue;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * sku销售属性&值 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface PmsSkuSaleAttrValueMapper extends BaseMapper<PmsSkuSaleAttrValue> {

}
