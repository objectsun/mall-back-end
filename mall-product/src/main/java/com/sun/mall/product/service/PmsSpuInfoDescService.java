package com.sun.mall.product.service;

import com.sun.mall.product.entity.PmsSpuInfoDesc;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * spu信息介绍 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface PmsSpuInfoDescService extends IService<PmsSpuInfoDesc> {

    void saveSpuInfoDesc(PmsSpuInfoDesc spuInfoDesc);
}
