package com.sun.mall.product.to;

import com.sun.mall.product.vo.spu.MemberPrice;
import lombok.Data;
import org.aspectj.lang.annotation.DeclareAnnotation;

import java.math.BigDecimal;
import java.util.List;

@Data
public class SkuReductionTo {

    private Long skuId;
    private int fullCount;
    private BigDecimal discount;
    private int countStatus;
    private BigDecimal fullPrice;
    private BigDecimal reducePrice;
    private int priceStatus;

    private List<MemberPrice> memberPrices;
}
