/**
  * Copyright 2023 bejson.com 
  */
package com.sun.mall.product.vo.spu;

import lombok.Data;

/**
 * Auto-generated: 2023-02-17 13:58:49
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class Images {

    private String imgUrl;
    private int defaultImg;

}