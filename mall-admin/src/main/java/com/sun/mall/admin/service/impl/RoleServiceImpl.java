package com.sun.mall.admin.service.impl;

import com.sun.mall.admin.entity.Role;
import com.sun.mall.admin.mapper.RoleMapper;
import com.sun.mall.admin.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

}
