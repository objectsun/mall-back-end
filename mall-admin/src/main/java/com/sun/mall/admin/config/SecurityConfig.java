package com.sun.mall.admin.config;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.mall.admin.component.CustomizeAuthenticationEntryPoint;
import com.sun.mall.admin.entity.User;
import com.sun.mall.admin.filter.JwtAuthenticationFilter;
import com.sun.mall.admin.filter.LoginFilter;
import com.sun.mall.admin.service.UserService;
import com.sun.mall.admin.service.impl.security.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserService userService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private JwtAuthenticationFilter jwtAuthenticationFilter;

    @Autowired
    private final UserDetailsService userDetailsService;
    @Autowired
    private final CustomizeAuthenticationEntryPoint customizeAuthenticationEntryPoint;

    public SecurityConfig(UserDetailsService userDetailsService, CustomizeAuthenticationEntryPoint customizeAuthenticationEntryPoint) {
        this.userDetailsService = userDetailsService;
        this.customizeAuthenticationEntryPoint = customizeAuthenticationEntryPoint;
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Bean
    public LoginFilter loginFilter() throws Exception {
        LoginFilter loginFilter = new LoginFilter(userService,tokenService);
        loginFilter.setUsernameParameter("username");
        loginFilter.setPasswordParameter("password");
        loginFilter.setFilterProcessesUrl("/security/user/login");
        loginFilter.setAuthenticationManager(authenticationManagerBean());
        loginFilter.setAuthenticationSuccessHandler((req,resp,authentication) -> {
            Map<String, Object> result = new HashMap<>();
            result.put("msg","登录成功");
            result.put("status", HttpStatus.OK.value());
            result.put("username", ((User)authentication.getPrincipal()).getUsername());
            User user = userService.getOne(Wrappers.<User>lambdaQuery().eq(User::getUsername, ((User)authentication.getPrincipal()).getUsername()));
            String token = TokenService.createToken((long) user.getId(), user.getUsername());
            result.put("token",token);
            resp.setContentType("application/json;charset=UTF-8");
            String s = new ObjectMapper().writeValueAsString(result);
            PrintWriter out = resp.getWriter();
            out.write(s);
            out.flush();
            out.close();
        });
        loginFilter.setAuthenticationFailureHandler((req,resp,ex) -> {
            Map<String, Object> result = new HashMap<>();
            result.put("msg",ex.getMessage());
            result.put("status",HttpStatus.INTERNAL_SERVER_ERROR.value());
            resp.setContentType("application/json;charset=UTF-8");
            String s = new ObjectMapper().writeValueAsString(result);
            PrintWriter out = resp.getWriter();
            out.write(s);
            out.flush();
            out.close();
        });
        return loginFilter;
    }

    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .mvcMatchers("/login").permitAll()
                .anyRequest().authenticated()//所以请求需要认证
                .and().exceptionHandling().
                authenticationEntryPoint(customizeAuthenticationEntryPoint)//匿名用户访问无权限资源时的异常处理
                .and()
                .formLogin().permitAll()
                .and()
                .cors() //跨域处理方案
                .configurationSource(configurationSource())
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .csrf().disable();

        http.addFilterAt(loginFilter(), UsernamePasswordAuthenticationFilter.class).addFilterBefore(jwtAuthenticationFilter,UsernamePasswordAuthenticationFilter.class);
    }

    public CorsConfigurationSource configurationSource() {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.setAllowedHeaders(Arrays.asList("*"));
        corsConfiguration.setAllowedMethods(Arrays.asList("*"));
        corsConfiguration.setAllowedOrigins(Arrays.asList("*"));
        corsConfiguration.setMaxAge(3600L);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", corsConfiguration);
        return source;
    }
}
