package com.sun.mall.admin.service.impl;

import com.sun.mall.admin.entity.User;
import com.sun.mall.admin.mapper.UserMapper;
import com.sun.mall.admin.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Override
    public boolean login(User user) {
        return false;
    }
}
