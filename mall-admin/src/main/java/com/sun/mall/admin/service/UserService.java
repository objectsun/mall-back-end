package com.sun.mall.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sun.mall.admin.entity.User;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author test_java
 * @since 2023-01-12
 */
public interface UserService extends IService<User> {

    boolean login(User user);
}
