package com.sun.mall.admin.service.impl.security;

import com.sun.mall.admin.common.ResultCode;
import com.sun.mall.admin.exception.BizRuntimeException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.CompressionCodecs;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.crypto.SecretKey;
import java.security.Key;
import java.util.Date;

/**
 * @Author:zhq
 * @Date:2022/11/22 14:21
 * @Version:1.0
 **/
@Component
public class TokenService {
    /**
     * 默认过期时间，单位毫秒
     */
    private static long TOKEN_EXPIRATION = 7*24*60*60*1000;

    /**
     * 服务器端存储秘钥
     */
    private static String TOKEN_SIGN_KEY = "cytogenetics20221122qwruhbgvfrthjkloiedsxcaqwerswqsz146t89543rgyyuijvfcawefsa";


    private static Key getKeyInstance(){
        //将秘钥转换成字节数组
        byte[] bytes = Decoders.BASE64URL.decode(TOKEN_SIGN_KEY);
        //使用hmac-sha算法生成签名key
        SecretKey secretKey = Keys.hmacShaKeyFor(bytes);
        return secretKey;
    }

    public static String createToken(Long userId, String userName) {
        String token = Jwts.builder()
                //指定主体，只是一种标识，没什么用
                .setSubject("CYTOGENETICS-USER")
                //指定过期时间
                .setExpiration(new Date(System.currentTimeMillis() + TOKEN_EXPIRATION))
                //jwt的载荷部分，可以在这里存放用户非敏感信息
                .claim("userId", userId)
                .claim("userName", userName)
                //签名
                .signWith(getKeyInstance())
                //指定压缩方式，这个设置要不要都行，因为我们这的claim不大，就几个数据，大的话就按需进行压缩
                .compressWith(CompressionCodecs.GZIP)
                //生成token
                .compact();
        return token;
    }

    /**
     * 校验token，虽然这里代码并没有体现校验了过期时间，但是经过实测，确实是校验了
     * @param token
     * @return
     */
    public static   boolean checkToken(String token) {
        if(!StringUtils.hasText(token)) {
            return false;
        }
        try {
            //验签名
            Jwts.parserBuilder().setSigningKey(getKeyInstance()).build().parseClaimsJws(token);
            //只要能走到这一步，就是成功了，如果不能的话，那就进了异常，直接返回错误即可
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 获取载荷，也就是前面生成token时存放的一些跟用户相关的信息
     * @param token
     * @return
     */
    private Claims getClaims(String token) {
        if(!StringUtils.hasText(token)) {
            // LOGIN_AUTH_ERROR(-211, "未登录"),
            throw BizRuntimeException.from(ResultCode.ERROR,"登录失败");
        }
        try {
            Jws<Claims> jws = Jwts.parserBuilder().setSigningKey(getKeyInstance()).build().parseClaimsJws(token);
            return jws.getBody();
        } catch (Exception e) {
            throw BizRuntimeException.from(ResultCode.ERROR,"认证信息异常");
        }
    }

    /**
     * 获取token中存的用户id
     * @param token
     * @return
     */
    public  Long getUserId(String token) {
        //解析token,获取里面的id信息
        Claims claims = getClaims(token);
        Integer userId = (Integer)claims.get("userId");
        return userId.longValue();
    }
}
