package com.sun.mall.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sun.mall.admin.entity.UserRole;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author test_java
 * @since 2023-01-12
 */
public interface UserRoleService extends IService<UserRole> {

}
