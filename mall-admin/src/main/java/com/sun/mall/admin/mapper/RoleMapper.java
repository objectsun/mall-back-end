package com.sun.mall.admin.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sun.mall.admin.entity.Role;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author test_java
 * @since 2023-01-12
 */
public interface RoleMapper extends BaseMapper<Role> {

}
