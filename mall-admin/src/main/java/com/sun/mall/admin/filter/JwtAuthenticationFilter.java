package com.sun.mall.admin.filter;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.sun.mall.admin.service.UserService;
import com.sun.mall.admin.service.impl.security.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author:zhq
 * @Date:2022/11/22 14:19
 * @Version:1.0
 **/
@Component
@Slf4j
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private static final AntPathMatcher PATH_MATCHER = new AntPathMatcher();

    private static String[] paths = {
            "/api/user/login",
            "/api/user/adminLogin",
            "/api/user/register",
            "/api/user/captchaImage",
            "/api/file/downloadPhoto/**",
            "/api/question/getQuestionPage",
            "/api/article/getArticlePage",
            "/api/article/getArticleDetailByIdWithNoLogin",
            "/api/question/getQuestionByIdWithNoLogin",
            "/api/comment/getCommentsAndReply"
    };

    @Autowired
    private TokenService tokenService;

    @Autowired
    private UserService userService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        //判断本次请求是否需要拦截
        if (!needInterceptor(request.getRequestURI())) {
            //先校验请求是否携带的token
            String token = request.getHeader("token");
            if (StringUtils.hasText(token)) {
                //如果有的话，则给本次线程存放用户认证信息
                Long userId = tokenService.getUserId(token);
                //这里设置了在Security中放入了userId做为principal,表明该用户已认证通过,可以根据用户id再去数据库中查询用户信息
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                        new UsernamePasswordAuthenticationToken(userId, null, null);
                //保存认证信息，后面投票器会给本次请求投放行票，暂时还没弄清楚投票器是根据什么来判断的
                //如果不给本次请求放这个，SpringSecurity会放一个匿名的认证信息，投票器会给这个投拒绝票
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }
        }
        filterChain.doFilter(request,response);
    }

    public boolean needInterceptor(String url){
        for (String path : paths) {
            //返回true，则说明不需要Authorization
            boolean match = PATH_MATCHER.match(path, url);
            if (match){
                return true;
            }
        }
        //如果一直返回false，则说明需要Authorization
        return false;
    }
}
