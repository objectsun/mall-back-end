package com.sun.mall.admin.service.impl;

import com.sun.mall.admin.entity.UserRole;
import com.sun.mall.admin.mapper.UserRoleMapper;
import com.sun.mall.admin.service.UserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

}
