package com.sun.mall.admin.controller;


import com.sun.mall.admin.common.Result;
import com.sun.mall.admin.entity.User;
import com.sun.mall.admin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author test_java
 * @since 2023-01-12
 */
@RestController
@RequestMapping("/security/user")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/unLogin")
    public Result<Object> unLogin() {
        return Result.ok("未登录");
    }

    @GetMapping("/getUsers")
    public Result<Object> getUsers() {
        List<User> users = userService.list();
        return Result.ok(users);
    }
}

