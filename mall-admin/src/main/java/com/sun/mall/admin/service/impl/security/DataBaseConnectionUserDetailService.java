package com.sun.mall.admin.service.impl.security;

import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.sun.mall.admin.common.ResultCode;
import com.sun.mall.admin.entity.Role;
import com.sun.mall.admin.entity.User;
import com.sun.mall.admin.entity.UserRole;
import com.sun.mall.admin.exception.BizRuntimeException;
import com.sun.mall.admin.service.RoleService;
import com.sun.mall.admin.service.UserRoleService;
import com.sun.mall.admin.service.UserService;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Wrapper;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DataBaseConnectionUserDetailService implements UserDetailsService {

    private final UserService userService;

    private final RoleService roleService;

    private final UserRoleService userRoleService;

    @Autowired
    public DataBaseConnectionUserDetailService(UserService userService, RoleService roleService, UserRoleService userRoleService) {
        this.userService = userService;
        this.roleService = roleService;
        this.userRoleService = userRoleService;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.getOne(Wrappers.<User>lambdaQuery().eq(User::getUsername, username));
        if (ObjectUtils.isEmpty(user)){
            throw BizRuntimeException.from(ResultCode.ERROR,"用户不存在");
        }
        List<UserRole> userRoles = userRoleService.list(Wrappers.<UserRole>lambdaQuery().eq(UserRole::getRid, user.getId()));
        List<Integer> roleIds = userRoles.stream().map(userRole -> userRole.getRid()).collect(Collectors.toList());
        List<Role> roles = roleService.list(Wrappers.<Role>lambdaQuery().in(Role::getId, roleIds));
        user.setRoles(roles);
        return user;
    }
}
