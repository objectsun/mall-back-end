package com.sun.mall.member.service;

import com.sun.mall.member.entity.UmsMemberLoginLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员登录记录 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface UmsMemberLoginLogService extends IService<UmsMemberLoginLog> {

}
