package com.sun.mall.member.service.impl;

import com.sun.mall.member.entity.UmsMemberStatisticsInfo;
import com.sun.mall.member.mapper.UmsMemberStatisticsInfoMapper;
import com.sun.mall.member.service.UmsMemberStatisticsInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员统计信息 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class UmsMemberStatisticsInfoServiceImpl extends ServiceImpl<UmsMemberStatisticsInfoMapper, UmsMemberStatisticsInfo> implements UmsMemberStatisticsInfoService {

}
