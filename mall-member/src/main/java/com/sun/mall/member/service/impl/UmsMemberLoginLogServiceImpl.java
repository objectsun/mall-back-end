package com.sun.mall.member.service.impl;

import com.sun.mall.member.entity.UmsMemberLoginLog;
import com.sun.mall.member.mapper.UmsMemberLoginLogMapper;
import com.sun.mall.member.service.UmsMemberLoginLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员登录记录 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class UmsMemberLoginLogServiceImpl extends ServiceImpl<UmsMemberLoginLogMapper, UmsMemberLoginLog> implements UmsMemberLoginLogService {

}
