package com.sun.mall.member.service.impl;

import com.sun.mall.member.entity.UmsMember;
import com.sun.mall.member.mapper.UmsMemberMapper;
import com.sun.mall.member.service.UmsMemberService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class UmsMemberServiceImpl extends ServiceImpl<UmsMemberMapper, UmsMember> implements UmsMemberService {

}
