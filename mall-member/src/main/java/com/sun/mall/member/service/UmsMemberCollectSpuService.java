package com.sun.mall.member.service;

import com.sun.mall.member.entity.UmsMemberCollectSpu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员收藏的商品 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface UmsMemberCollectSpuService extends IService<UmsMemberCollectSpu> {

}
