package com.sun.mall.member.service;

import com.sun.mall.member.entity.UmsIntegrationChangeHistory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 积分变化历史记录 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface UmsIntegrationChangeHistoryService extends IService<UmsIntegrationChangeHistory> {

}
