package com.sun.mall.member.controller;


import com.sun.mall.member.common.Result;
import com.sun.mall.member.feign.CouponFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

/**
 * <p>
 * 会员 前端控制器
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@RestController
@RequestMapping("/member/ums-member")
public class UmsMemberController {

    @Autowired
    private CouponFeignService couponFeignService;

    @GetMapping("/coupons")
    public Result<Object> test() {
        Result<Object> result = couponFeignService.memberCoupons();
        return Result.ok(result);
    }
}

