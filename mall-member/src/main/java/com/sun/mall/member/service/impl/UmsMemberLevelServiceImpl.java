package com.sun.mall.member.service.impl;

import com.sun.mall.member.entity.UmsMemberLevel;
import com.sun.mall.member.mapper.UmsMemberLevelMapper;
import com.sun.mall.member.service.UmsMemberLevelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员等级 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class UmsMemberLevelServiceImpl extends ServiceImpl<UmsMemberLevelMapper, UmsMemberLevel> implements UmsMemberLevelService {

}
