package com.sun.mall.member.mapper;

import com.sun.mall.member.entity.UmsMemberLevel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员等级 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface UmsMemberLevelMapper extends BaseMapper<UmsMemberLevel> {

}
