package com.sun.mall.member.service.impl;

import com.sun.mall.member.entity.UmsMemberCollectSubject;
import com.sun.mall.member.mapper.UmsMemberCollectSubjectMapper;
import com.sun.mall.member.service.UmsMemberCollectSubjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员收藏的专题活动 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class UmsMemberCollectSubjectServiceImpl extends ServiceImpl<UmsMemberCollectSubjectMapper, UmsMemberCollectSubject> implements UmsMemberCollectSubjectService {

}
