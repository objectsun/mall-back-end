package com.sun.mall.member.mapper;

import com.sun.mall.member.entity.UmsGrowthChangeHistory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 成长值变化历史记录 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface UmsGrowthChangeHistoryMapper extends BaseMapper<UmsGrowthChangeHistory> {

}
