package com.sun.mall.member.mapper;

import com.sun.mall.member.entity.UmsMemberCollectSpu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员收藏的商品 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface UmsMemberCollectSpuMapper extends BaseMapper<UmsMemberCollectSpu> {

}
