package com.sun.mall.member.service.impl;

import com.sun.mall.member.entity.UmsGrowthChangeHistory;
import com.sun.mall.member.mapper.UmsGrowthChangeHistoryMapper;
import com.sun.mall.member.service.UmsGrowthChangeHistoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 成长值变化历史记录 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class UmsGrowthChangeHistoryServiceImpl extends ServiceImpl<UmsGrowthChangeHistoryMapper, UmsGrowthChangeHistory> implements UmsGrowthChangeHistoryService {

}
