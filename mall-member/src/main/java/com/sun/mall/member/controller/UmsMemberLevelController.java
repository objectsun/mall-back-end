package com.sun.mall.member.controller;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.sun.mall.member.common.Result;
import com.sun.mall.member.entity.UmsMemberLevel;
import com.sun.mall.member.service.UmsMemberLevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 会员等级 前端控制器
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@RestController
@RequestMapping("/member/ums-member-level")
public class UmsMemberLevelController {
    @Autowired
    private UmsMemberLevelService memberLevelService;
    @GetMapping("/getMemberLevels")
    public Result<Object> getMemberLevels(){
        List<UmsMemberLevel> memberLevels = memberLevelService.list();
        return Result.ok(memberLevels);
    }
    @GetMapping("/getMemberLevel")
    public Result<Object> getMemberLevel(Long levelId){
        UmsMemberLevel memberLevel = memberLevelService.getById(levelId);
        return Result.ok(memberLevel);
    }
}

