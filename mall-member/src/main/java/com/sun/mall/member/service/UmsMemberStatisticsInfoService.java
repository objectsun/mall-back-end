package com.sun.mall.member.service;

import com.sun.mall.member.entity.UmsMemberStatisticsInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员统计信息 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface UmsMemberStatisticsInfoService extends IService<UmsMemberStatisticsInfo> {

}
