package com.sun.mall.member.feign;

import com.sun.mall.member.common.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("mall-coupon")
public interface CouponFeignService {
    @GetMapping("/coupon/sms-coupon/member/list")
    public Result<Object> memberCoupons();
}
