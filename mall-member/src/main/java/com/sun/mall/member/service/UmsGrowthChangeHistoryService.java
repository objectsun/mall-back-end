package com.sun.mall.member.service;

import com.sun.mall.member.entity.UmsGrowthChangeHistory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 成长值变化历史记录 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface UmsGrowthChangeHistoryService extends IService<UmsGrowthChangeHistory> {

}
