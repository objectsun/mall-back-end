package com.sun.mall.member.service.impl;

import com.sun.mall.member.entity.UmsMemberCollectSpu;
import com.sun.mall.member.mapper.UmsMemberCollectSpuMapper;
import com.sun.mall.member.service.UmsMemberCollectSpuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员收藏的商品 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class UmsMemberCollectSpuServiceImpl extends ServiceImpl<UmsMemberCollectSpuMapper, UmsMemberCollectSpu> implements UmsMemberCollectSpuService {

}
