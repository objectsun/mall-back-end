package com.sun.mall.member.service.impl;

import com.sun.mall.member.entity.UmsMemberReceiveAddress;
import com.sun.mall.member.mapper.UmsMemberReceiveAddressMapper;
import com.sun.mall.member.service.UmsMemberReceiveAddressService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员收货地址 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class UmsMemberReceiveAddressServiceImpl extends ServiceImpl<UmsMemberReceiveAddressMapper, UmsMemberReceiveAddress> implements UmsMemberReceiveAddressService {

}
