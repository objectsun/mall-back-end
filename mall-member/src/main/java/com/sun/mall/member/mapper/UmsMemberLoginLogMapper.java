package com.sun.mall.member.mapper;

import com.sun.mall.member.entity.UmsMemberLoginLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员登录记录 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface UmsMemberLoginLogMapper extends BaseMapper<UmsMemberLoginLog> {

}
