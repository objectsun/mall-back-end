package com.sun.mall.member.service;

import com.sun.mall.member.entity.UmsMemberLevel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员等级 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface UmsMemberLevelService extends IService<UmsMemberLevel> {

}
