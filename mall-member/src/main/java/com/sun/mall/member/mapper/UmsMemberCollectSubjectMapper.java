package com.sun.mall.member.mapper;

import com.sun.mall.member.entity.UmsMemberCollectSubject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员收藏的专题活动 Mapper 接口
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface UmsMemberCollectSubjectMapper extends BaseMapper<UmsMemberCollectSubject> {

}
