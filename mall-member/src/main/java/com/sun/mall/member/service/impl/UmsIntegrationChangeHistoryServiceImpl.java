package com.sun.mall.member.service.impl;

import com.sun.mall.member.entity.UmsIntegrationChangeHistory;
import com.sun.mall.member.mapper.UmsIntegrationChangeHistoryMapper;
import com.sun.mall.member.service.UmsIntegrationChangeHistoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 积分变化历史记录 服务实现类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
@Service
public class UmsIntegrationChangeHistoryServiceImpl extends ServiceImpl<UmsIntegrationChangeHistoryMapper, UmsIntegrationChangeHistory> implements UmsIntegrationChangeHistoryService {

}
