package com.sun.mall.member.service;

import com.sun.mall.member.entity.UmsMemberReceiveAddress;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员收货地址 服务类
 * </p>
 *
 * @author ^tyro^
 * @since 2023-02-08
 */
public interface UmsMemberReceiveAddressService extends IService<UmsMemberReceiveAddress> {

}
